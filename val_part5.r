# INFO --------------------------------------------------------------------
# Project: Value
# Author: Vignan Reddy Thumu
# Date: 06/13/20
# Description: 

# # load in the necessary LIBRARIES ---------------------------------------------------------------
#library(foreign) ## to load in sav file
library(dplyr)
library(tidyverse)


library(haven)
library(readr)
library(stringr)
library(readxl)
library(fuzzyjoin)
library(lubridate)
library(stringr)

library(qdapRegex) ## to extract strings between 


#clear the global enviroment
rm(list = ls())

## set the working directory..
setwd("C:/Users/vthumu/OneDrive - Winsight/Projects/Value LTO/value-lto")
# LOAD DATA ---------------------------------------------------------------
lto <- read_sav("VAL_MAY2020.sav")

lto_june <- read_sav("VAL_JUNE2020.sav")

lto_july <- read_sav("VAL_JULY2020.sav")

lto_aug <- read_sav("VAL_AUG2020.sav")


# change 
lto_june$Respondent_Serial <- paste0(lto_june$Respondent_Serial, "1111")


# change 
lto_july$Respondent_Serial <- paste0(lto_july$Respondent_Serial, "2222")


# change 
lto_aug$Respondent_Serial <- paste0(lto_aug$Respondent_Serial, "3333")




#View(data.frame(names(lto)))

## convert into date format
lto$StartingTime <- as.Date(lto$StartingTime, "%m/%d/%y")

## convert into date format
lto_june$StartingTime <- as.Date(lto_june$StartingTime, "%m/%d/%y")


## convert into date format
lto_july$StartingTime <- as.Date(lto_july$StartingTime, "%m/%d/%y")


## convert into date format
lto_aug$StartingTime <- as.Date(lto_aug$StartingTime, "%m/%d/%y")



unique(lto_aug$StartingTime)



## only may Id's 
may_Id <- lto %>%
  filter(StartingTime >= "2020-05-01" & StartingTime <= "2020-05-31") %>%
  pull(Respondent_Serial)


## only june Id's 
june_Id <- lto_june %>%
  filter(StartingTime >= "2020-06-01" & StartingTime <= "2020-06-30") %>%
  pull(Respondent_Serial)


## only june Id's 
july_Id <- lto_july %>%
  filter(StartingTime >= "2020-07-01" & StartingTime <= "2020-07-31") %>%
  pull(Respondent_Serial)


## only Aug Id's 
aug_Id <- lto_aug %>%
  filter(StartingTime >= "2020-08-01" & StartingTime <= "2020-08-31") %>%
  pull(Respondent_Serial)




#View(data.frame(names(lto_june)))

#dataset = read.spss("VAL_MAY2020.sav", to.data.frame=TRUE)

#str(df, list.len=ncol(df))

# save(df1, df2, df3, file = 'file_name.Rda')
#View(data.frame(names(dataset)))
# #View(final_output[,c(1,233:260)])
# #View(data.frame(names(df)))
#View(data.frame(names(checkQ12_march)))
#View(data.frame(names(filer_lto)))
#View(data.frame(names(household_segment_QHfinal_value)))
# #data.frame(levels(final_output3$segment))

#TecseIpaclosattentomenpricsthaIcafinthbesvalufommoneSinRespoQues - pay close attention to menu prices to find best value 
#Techseg_Iusuallypickrestaurantwith_lowerpriceSingleResponQuestio - usally pick resturants with lower prices 
#Teserarseourestathofdiscorspedeeg2f1dekidefrdiscomepretSinResQue - Seek out restaurants that offer discounts or special deals


## june filter
filer_lto_june <- lto_june %>%
  select(Respondent_Serial, Gender, Age, Region, starts_with("Ethnic"), Household1, Household2, Household3, Household4, Household5, Household6, Household7, Household8,
         Income, TecseIpaclosattentomenpricsthaIcafinthbesvalufommoneSinRespoQues, Techseg_Iusuallypickrestaurantwith_lowerpriceSingleResponQuestio, 
         Teserarseourestathofdiscorspedeeg2f1dekidefrdiscomepretSinResQue, starts_with("QG"))



## july
filer_lto_july <- lto_july %>%
  select(Respondent_Serial, Gender, Age, Region, starts_with("Ethnic"), Household1, Household2, Household3, Household4, Household5, Household6, Household7, Household8,
         Income, TecseIpaclosattentomenpricsthaIcafinthbesvalufommoneSinRespoQues, Techseg_Iusuallypickrestaurantwith_lowerpriceSingleResponQuestio, 
         Teserarseourestathofdiscorspedeeg2f1dekidefrdiscomepretSinResQue, starts_with("QG"))


## Aug
filer_lto_aug <- lto_aug %>%
  select(Respondent_Serial, Gender, Age, Region, starts_with("Ethnic"), Household1, Household2, Household3, Household4, Household5, Household6, Household7, Household8,
         Income, TecseIpaclosattentomenpricsthaIcafinthbesvalufommoneSinRespoQues, Techseg_Iusuallypickrestaurantwith_lowerpriceSingleResponQuestio, 
         Teserarseourestathofdiscorspedeeg2f1dekidefrdiscomepretSinResQue, starts_with("QG"))


## remove 8, 9 for QH filter
filer_lto <- lto %>%
              select(Respondent_Serial, Gender, Age, Region, starts_with("Ethnic"), Household1, Household2, Household3, Household4, Household5, Household6, Household7, Household8,
                     Income, TecseIpaclosattentomenpricsthaIcafinthbesvalufommoneSinRespoQues, Techseg_Iusuallypickrestaurantwith_lowerpriceSingleResponQuestio, 
                      Teserarseourestathofdiscorspedeeg2f1dekidefrdiscomepretSinResQue, starts_with("QG"), -QG_Movie_Theater_Concessions_SingleResponseQuestion)

## combine May and June data
filer_lto <- rbind(filer_lto, filer_lto_june, filer_lto_july, filer_lto_aug)


## change col names for value seekers values 
colnames(filer_lto)[colnames(filer_lto) == 'TecseIpaclosattentomenpricsthaIcafinthbesvalufommoneSinRespoQues'] <- 'Pay_attention'
colnames(filer_lto)[colnames(filer_lto) == 'Techseg_Iusuallypickrestaurantwith_lowerpriceSingleResponQuestio'] <- 'lower_prices'
colnames(filer_lto)[colnames(filer_lto) == 'Teserarseourestathofdiscorspedeeg2f1dekidefrdiscomepretSinResQue'] <- 'deals'

## change col names for QG
names(filer_lto)[19]<-'QuickService'
names(filer_lto)[20]<-'FastCasual'
names(filer_lto)[21]<-'Midscale'
names(filer_lto)[22]<-'Casualdining'
names(filer_lto)[23]<-'Conveniencestores'
#names(filer_lto)[24]<-'Movie_Theater'
names(filer_lto)[24]<-'Independent'


## household2 - Spouse/significant other
## household3 - Children under 12
# household5 - Adult children 
# household7 - friends/non related roommates
# household8 - Other

## change col names for households
colnames(filer_lto)[colnames(filer_lto) == 'Household1'] <- 'Just_myself'
colnames(filer_lto)[colnames(filer_lto) == 'Household2'] <- 'Spouse_significant_other'
colnames(filer_lto)[colnames(filer_lto) == 'Household3'] <- 'Children_under_12'
colnames(filer_lto)[colnames(filer_lto) == 'Household4'] <- 'children_12_17'
colnames(filer_lto)[colnames(filer_lto) == 'Household5'] <- 'Adult_children'
colnames(filer_lto)[colnames(filer_lto) == 'Household6'] <- 'other_adults'
colnames(filer_lto)[colnames(filer_lto) == 'Household7'] <- 'friends_non_related_roommates'
colnames(filer_lto)[colnames(filer_lto) == 'Household8'] <- 'Others'



# Demographics ------------------------------------------------------------
demographic_filter <- filer_lto %>%
                      select(Respondent_Serial, Gender, Age, Region, Ethnicity, Income)



glimpse(demographic_filter)

demographic_filter$Gender <- gsub("1", "Male", demographic_filter$Gender)
demographic_filter$Gender <- gsub("2", "Female", demographic_filter$Gender)


unique(demographic_filter$Gender)

##  Generation
demographic_filter$Generation <- ifelse(demographic_filter$Age <= 26, "Gen Z", 
                              ifelse(demographic_filter$Age >= 27 & demographic_filter$Age <= 43, "Millennials", 
                                     ifelse(demographic_filter$Age >= 44 & demographic_filter$Age <= 54, "Generation X", 
                                            ifelse(demographic_filter$Age >= 55 & demographic_filter$Age <= 74, "Baby Boomers", 
                                                   ifelse(demographic_filter$Age >= 75, "Mature", demographic_filter$Age)))))

unique(demographic_filter$Age)

## AgeGroup
demographic_filter$AgeGroup <- ifelse(demographic_filter$Age <= 17, "< 17", 
                                      ifelse(demographic_filter$Age >= 18 & demographic_filter$Age <= 24, "18-24", 
                                             ifelse(demographic_filter$Age >= 25 & demographic_filter$Age <= 34, "25-34", 
                                                    ifelse(demographic_filter$Age >= 34 & demographic_filter$Age <= 44, "34-44", 
                                                           ifelse(demographic_filter$Age >= 45 & demographic_filter$Age <= 54, "45-54", 
                                                               ifelse(demographic_filter$Age >= 55, "55+", demographic_filter$Age))))))

unique(demographic_filter$AgeGroup)
##Region wait on this 
unique(demographic_filter$Region)


## Ethincity
unique(demographic_filter$Ethnicity)

demographic_filter$Ethnicity <- gsub("1", "Asian", demographic_filter$Ethnicity)
demographic_filter$Ethnicity <- gsub("2", "Black/African-American", demographic_filter$Ethnicity)
demographic_filter$Ethnicity <- gsub("3", "Caucasian", demographic_filter$Ethnicity)
demographic_filter$Ethnicity <- gsub("4", "Hispanic", demographic_filter$Ethnicity)
demographic_filter$Ethnicity <- gsub("5", "Mixed", demographic_filter$Ethnicity)
demographic_filter$Ethnicity <- gsub("6", "Other", demographic_filter$Ethnicity)

unique(demographic_filter$Ethnicity)

# demographic_filter <- demographic_filter %>%
#                       filter(!Ethnicity %in% c("Other", "Mixed"))


## income
unique(demographic_filter$Income)

glimpse(demographic_filter)

demographic_filter$Income <- as.character(demographic_filter$Income)

## AgeGroup
demographic_filter$Income2 <- ifelse(demographic_filter$Income == "1", "under 30,000", 
                                      ifelse(demographic_filter$Income == "2", "30,000 - 44,000", 
                                             ifelse(demographic_filter$Income == "3", "45,000 - 64,000", 
                                                    ifelse(demographic_filter$Income == "4", "65,000 - 74,000", 
                                                           ifelse(demographic_filter$Income == "5", "75,000 - 99,000", "100,00+")))))
unique(demographic_filter$Income2)


demographic_filter$Income2 <- factor(demographic_filter$Income2, levels = c("under 30,000", "30,000 - 44,000", 
                                                                            "45,000 - 64,000","65,000 - 74,000", "75,000 - 99,000", "100,00+"))


glimpse(filer_lto)

# Household_filter --------------------------------------------------------

filter_household <- filer_lto %>%
                    select(Respondent_Serial, 
                           Just_myself, 
                           Spouse_significant_other, 
                           Children_under_12, 
                           children_12_17, 
                           Adult_children,  other_adults, friends_non_related_roommates, Others) #%>%
                    #filter(Household1 != 0 | Household4 != 0 | Household6 != 0)


filter_household$younger_17_main <- ifelse(filter_household$Children_under_12 | filter_household$children_12_17 == 1 , 1, 0)

filter_household$other_adults_main <- ifelse(filter_household$Spouse_significant_other | 
                                               filter_household$Adult_children | filter_household$other_adults | filter_household$friends_non_related_roommates
                                             | filter_household$Others  == 1 , 1, 0)

glimpse(filter_household)

filter_household <- filter_household %>%
                    select(1, Just_myself, younger_17_main, other_adults_main) %>%
  filter(Just_myself != 0 | younger_17_main != 0 | other_adults_main != 0)


colnames(filter_household)[colnames(filter_household) == 'younger_17_main'] <- 'younger_17'
colnames(filter_household)[colnames(filter_household) == 'other_adults_main'] <- 'other_adults'



# value seekers filter ----------------------------------------------------
filter_value_seeker <- filer_lto %>%
  select(Respondent_Serial, Pay_attention, lower_prices, deals)



# filter_value_seeker_long <- filter_value_seeker %>%
#   gather(value_seeker, value, c(Pay_attention, lower_prices, deals)) %>%
#   filter(value != 3)

filter_value_seeker_long <- filter_value_seeker %>%
  gather(value_seeker, value, c(Pay_attention, lower_prices, deals))

unique(filter_value_seeker_long$value)
unique(filter_value_seeker_long$value_seeker)

filter_value_seeker_long$value <- as.character(filter_value_seeker_long$value)



filter_value_seeker_long$value_seeker <- ifelse(filter_value_seeker_long$value == "3", "Never", filter_value_seeker_long$value_seeker)

filter_value_seeker_long <- filter_value_seeker_long %>%
                            select(-value) %>%
                            distinct()

# Id_test_seeker <- filter_value_seeker_long %>%
#                   distinct(Respondent_Serial)


#glimpse(filter_value_seeker_long)

#filter_value_seeker_long$tech <- ifelse(filter_value_seeker_long$value %in% c(4,5), "agree_agreeCompletly", "disagreeCompletely_disagree")

#filter_value_seeker_long$tech <- ifelse(filter_value_seeker_long$value %in% c(4,5), "agree_agreeCompletly", ifelse(filter_value_seeker_long$value == 3, "Neither", "disagreeCompletely_disagree"))



# 1,2 disagreeCompletely_disagree
# 3 - Neither
# 4,5 agree_agreeCompletly

#|| (value_seeker == "deals"  && value %in% c(1,2)

# filter_value_seeker_long2 <- filter_value_seeker_long %>%
#                              filter(value_seeker %in% c("Pay_attention", "lower_prices")  & tech == "agree_agreeCompletly" | value_seeker == "deals"  & tech == "disagreeCompletely_disagree")
# 


# filter_value_seeker2 <- filter_value_seeker_long %>%
#                         select(-value, -tech) %>%
#                         mutate(x = 1) %>%
#                         spread(value_seeker, x) %>%
#                        mutate_all(funs(replace_na(.,0))) ## replace na's to 0



# filter_value_seeker2 <- filter_value_seeker_long %>%
#   select(-value, -tech) %>%
#   mutate(x = 1) %>%
#   spread(value_seeker, x) %>%
#   mutate_all(funs(replace_na(.,0))) ## replace na's to 0


## check for duplicates
#filter_value_seeker2$Respondent_Serial[duplicated(filter_value_seeker2$Respondent_Serial)]

## check for duplicates
#filter_value_seeker_long2$Respondent_Serial[duplicated(filter_value_seeker_long2$Respondent_Serial)]

glimpse(filer_lto)

# segment usage filter ----------------------------------------------------
filter_segment <- filer_lto %>%
  select(Respondent_Serial, QuickService, FastCasual, Midscale, Casualdining, Conveniencestores)
                              



#glimpse(filter_segment)

#### 1 - every day 2 - A couple times a week but not everyday, 3 - once a week 
## 4- two fo three times a month, 5 - once a month, 6-once every 2-3 months, 7 - less often than every 3 months
## 8 - Never

filter_segment_long <- filter_segment %>% 
  gather(segment, value, c(QuickService, FastCasual, Midscale, Casualdining, Conveniencestores)) #%>%
  #filter(value != 8)



filter_segment_long$segment <- ifelse(filter_segment_long$value == 8, "Never", filter_segment_long$segment)


filter_segment_long <- filter_segment_long %>%
                       select(-value) %>%
                       distinct()



# filter_segment2 <-  filter_segment_long %>%
#                     mutate(value = 1) %>%
#                     distinct() %>%
#                     spread(segment, value) %>%
#                     mutate_all(funs(replace_na(.,0))) ## replace na's to 0
  

#unique(filter_segment_long$value)

# QH filter ---------------------------------------------------- june below 

QH_june_casual2 <- lto_june %>%
  select(Respondent_Serial, starts_with("QHcasual2")) %>%
  mutate_all(funs(replace_na(.,0))) ## replace na's to 0

## why are you removing question 8 and question 9??
df1_long <- QH_june_casual2 %>% 
  gather(QH, value, -1) %>%
  drop_na() %>%
  filter(!grepl('Question8', QH)) %>%
  filter(!grepl('Question9', QH)) %>%
  filter(value == 1)


ID_tEst <- df1_long %>%
           distinct(Respondent_Serial)



# filter(!QH1 %in% c("QHcasual2_O_Charley_s_SingleResponseQuestion8" , "QHcasual2_O_Charley_s_SingleResponseQuestion9")) %>%
# mutate(value = 1) %>%
# mutate(QH1 = "Charleys")

df1_long2 <- df1_long %>%
  separate(QH, c("x", "y"), "_Single") %>%
  select(-y) %>%
  distinct()

df1_long2$x <- gsub("QHcasual2", "", df1_long2$x)

unique(df1_long2$x)


df1_long2$x <- gsub("_O_Charley_s", "O'Charley's", df1_long2$x)
df1_long2$x <- gsub("_Olive_Garden", "Olive Garden", df1_long2$x)
df1_long2$x <- gsub("_On_the_Border", "On the Border", df1_long2$x)
df1_long2$x <- gsub("_Outback_Steakhouse", "Outback Steakhouse", df1_long2$x)
df1_long2$x <- gsub("_P_F_Chang_s_China_Bistro", "P. F. Chang's China Bistro", df1_long2$x)
df1_long2$x <- gsub("_Red_Lobster", "Red Lobster", df1_long2$x)
df1_long2$x <- gsub("_Red_Robin_Gourmet_BurgersBrews", "Red Robin Gourmet BurgersBrews", df1_long2$x)
df1_long2$x <- gsub("_Ruby_Tuesday", "Ruby Tuesday", df1_long2$x)
df1_long2$x <- gsub("_Texas_Roadhouse", "Texas Roadhouse", df1_long2$x)
df1_long2$x <- gsub("_TGI_Fridays", "TGI Fridays", df1_long2$x)
df1_long2$x <- gsub("_The_Cheesecake_Factory", "The Cheesecake Factory", df1_long2$x)



df1_spread <- df1_long2 %>%
  spread(x, value) %>%
  mutate_all(funs(replace_na(.,0))) ## replace na's to 0



## to get the names for QH
QH_june <- lto_june %>%
  select(Respondent_Serial, starts_with("QH"), -starts_with("QHcasual2"))
  

unique(df1_long2$x)

#View(data.frame(names(QH_june)))


## get all the names for QH
question_chains_QH_june <- do.call(rbind, lapply(QH_june[, 2:98], function(x) attributes(x)$label)) %>% 
  as.data.frame(stringsAsFactors = FALSE) %>% 
  rownames_to_column() %>% 
  pull(V1)

question_chains_QH_june <- as.data.frame(question_chains_QH_june)

question_chains_QH_june$question_chains_QH_june <- gsub(':', '', question_chains_QH_june$question_chains_QH_june)

question_chains_QH_june$question_chains_QH_june <- trimws(question_chains_QH_june$question_chains_QH_june)

colnames(QH_june) <- c("Respondent_Serial",question_chains_QH_june$question_chains_QH_june)

QH_june_long <- QH_june %>% 
  gather(QH_june, value, -1) %>%
  drop_na() %>%
  filter(!value %in% c(8,9)) %>%
  mutate(value = 1)

filter_QH_june <- QH_june_long %>%
  spread(QH_june, value) %>%
  mutate_all(funs(replace_na(.,0))) ## replace na's to 0



## left join filter_QH_june and df1_spread

final_QH_june <- merge(filter_QH_june, df1_spread, by = c("Respondent_Serial"), all = T)


final_QH_june  <- final_QH_june  %>%
  mutate_all(funs(replace_na(.,0))) ## replace na's to 0


# QH filter ---------------------------------------------------- june above


# QH filter ---------------------------------------------------- july below 

QH_july_casual2 <- lto_july %>%
  select(Respondent_Serial, starts_with("QHcasual2")) %>%
  mutate_all(funs(replace_na(.,0))) ## replace na's to 0


df1_long_july <- QH_july_casual2 %>% 
  gather(QH, value, -1) %>%
  drop_na() %>%
  filter(!grepl('Question8', QH)) %>%
  filter(!grepl('Question9', QH)) %>%
  filter(value == 1)

# filter(!QH1 %in% c("QHcasual2_O_Charley_s_SingleResponseQuestion8" , "QHcasual2_O_Charley_s_SingleResponseQuestion9")) %>%
# mutate(value = 1) %>%
# mutate(QH1 = "Charleys")

df1_long2_july <- df1_long_july %>%
  separate(QH, c("x", "y"), "_Single") %>%
  select(-y) %>%
  distinct()

df1_long2_july$x <- gsub("QHcasual2", "", df1_long2_july$x)

unique(df1_long2_july$x)


df1_long2_july$x <- gsub("_O_Charley_s", "O'Charley's", df1_long2_july$x)
df1_long2_july$x <- gsub("_Olive_Garden", "Olive Garden", df1_long2_july$x)
df1_long2_july$x <- gsub("_On_the_Border", "On the Border", df1_long2_july$x)
df1_long2_july$x <- gsub("_Outback_Steakhouse", "Outback Steakhouse", df1_long2_july$x)
df1_long2_july$x <- gsub("_P_F_Chang_s_China_Bistro", "P. F. Chang's China Bistro", df1_long2_july$x)
df1_long2_july$x <- gsub("_Red_Lobster", "Red Lobster", df1_long2_july$x)
df1_long2_july$x <- gsub("_Red_Robin_Gourmet_BurgersBrews", "Red Robin Gourmet BurgersBrews", df1_long2_july$x)
df1_long2_july$x <- gsub("_Ruby_Tuesday", "Ruby Tuesday", df1_long2_july$x)
df1_long2_july$x <- gsub("_Texas_Roadhouse", "Texas Roadhouse", df1_long2_july$x)
df1_long2_july$x <- gsub("_TGI_Fridays", "TGI Fridays", df1_long2_july$x)
df1_long2_july$x <- gsub("_The_Cheesecake_Factory", "The Cheesecake Factory", df1_long2_july$x)



df1_spread_july <- df1_long2_july %>%
  spread(x, value) %>%
  mutate_all(funs(replace_na(.,0))) ## replace na's to 0



## to get names for QH
QH_july <- lto_july %>%
  select(Respondent_Serial, starts_with("QH"), -starts_with("QHcasual2")) %>%
  select(-75:-92) ## removing these because they are messed up


unique(df1_long2_july$x)

#View(data.frame(names(QH_july)))


## get all the names for QH
question_chains_QH_july <- do.call(rbind, lapply(QH_july[, 2:98], function(x) attributes(x)$label)) %>% 
  as.data.frame(stringsAsFactors = FALSE) %>% 
  rownames_to_column() %>% 
  pull(V1)

question_chains_QH_july <- as.data.frame(question_chains_QH_july)

question_chains_QH_july$question_chains_QH_july <- gsub(':', '', question_chains_QH_july$question_chains_QH_july)

question_chains_QH_july$question_chains_QH_july <- trimws(question_chains_QH_july$question_chains_QH_july)

colnames(QH_july) <- c("Respondent_Serial",question_chains_QH_july$question_chains_QH_july)


#x <- as.data.frame(colnames(QH_july))


QH_july_long <- QH_july %>% 
  gather(QH_july, value, -1) %>%
  drop_na() %>%
  filter(!value %in% c(8,9)) %>%
  mutate(value = 1)

filter_QH_july <- QH_july_long %>%
  spread(QH_july, value) %>%
  mutate_all(funs(replace_na(.,0))) ## replace na's to 0



## left join filter_QH_july and df1_spread_july

final_QH_july <- merge(filter_QH_july, df1_spread_july, by = c("Respondent_Serial"), all = T)


final_QH_july  <- final_QH_july  %>%
  mutate_all(funs(replace_na(.,0))) ## replace na's to 0

# QH filter ---------------------------------------------------- july above


# QH filter ---------------------------------------------------- Aug below 

QH_aug_casual2 <- lto_aug %>%
  select(Respondent_Serial, starts_with("QHcasual2")) %>%
  mutate_all(funs(replace_na(.,0))) ## replace na's to 0


df1_long_aug <- QH_aug_casual2 %>% 
  gather(QH, value, -1) %>%
  drop_na() %>%
  filter(!grepl('Question8', QH)) %>%
  filter(!grepl('Question9', QH)) %>%
  filter(value == 1)

# filter(!QH1 %in% c("QHcasual2_O_Charley_s_SingleResponseQuestion8" , "QHcasual2_O_Charley_s_SingleResponseQuestion9")) %>%
# mutate(value = 1) %>%
# mutate(QH1 = "Charleys")

df1_long2_aug <- df1_long_aug %>%
  separate(QH, c("x", "y"), "_Single") %>%
  select(-y) %>%
  distinct()

df1_long2_aug$x <- gsub("QHcasual2", "", df1_long2_aug$x)

unique(df1_long2_aug$x)


df1_long2_aug$x <- gsub("_O_Charley_s", "O'Charley's", df1_long2_aug$x)
df1_long2_aug$x <- gsub("_Olive_Garden", "Olive Garden", df1_long2_aug$x)
df1_long2_aug$x <- gsub("_On_the_Border", "On the Border", df1_long2_aug$x)
df1_long2_aug$x <- gsub("_Outback_Steakhouse", "Outback Steakhouse", df1_long2_aug$x)
df1_long2_aug$x <- gsub("_P_F_Chang_s_China_Bistro", "P. F. Chang's China Bistro", df1_long2_aug$x)
df1_long2_aug$x <- gsub("_Red_Lobster", "Red Lobster", df1_long2_aug$x)
## this got changed to _Red_Robin_Gourmet_Burgers_Brews
df1_long2_aug$x <- gsub("_Red_Robin_Gourmet_Burgers_Brews", "Red Robin Gourmet BurgersBrews", df1_long2_aug$x)
df1_long2_aug$x <- gsub("_Ruby_Tuesday", "Ruby Tuesday", df1_long2_aug$x)
df1_long2_aug$x <- gsub("_Texas_Roadhouse", "Texas Roadhouse", df1_long2_aug$x)
df1_long2_aug$x <- gsub("_TGI_Fridays", "TGI Fridays", df1_long2_aug$x)
df1_long2_aug$x <- gsub("_The_Cheesecake_Factory", "The Cheesecake Factory", df1_long2_aug$x)



df1_spread_aug <- df1_long2_aug %>%
  spread(x, value) %>%
  mutate_all(funs(replace_na(.,0))) ## replace na's to 0



## to get names for QH
QH_aug <- lto_aug %>%
  select(Respondent_Serial, starts_with("QH"), -starts_with("QHcasual2")) #%>%
  #select(-75:-92) ## removing these because they are messed up


unique(df1_long2_aug$x)

#View(data.frame(names(QH_aug)))


## get all the names for QH
question_chains_QH_aug <- do.call(rbind, lapply(QH_aug[, 2:98], function(x) attributes(x)$label)) %>% 
  as.data.frame(stringsAsFactors = FALSE) %>% 
  rownames_to_column() %>% 
  pull(V1)

question_chains_QH_aug <- as.data.frame(question_chains_QH_aug)

question_chains_QH_aug$question_chains_QH_aug <- gsub(':', '', question_chains_QH_aug$question_chains_QH_aug)

question_chains_QH_aug$question_chains_QH_aug <- trimws(question_chains_QH_aug$question_chains_QH_aug)

colnames(QH_aug) <- c("Respondent_Serial",question_chains_QH_aug$question_chains_QH_aug)


#x <- as.data.frame(colnames(QH_aug))


QH_aug_long <- QH_aug %>% 
  gather(QH_aug, value, -1) %>%
  drop_na() %>%
  filter(!value %in% c(8,9)) %>%
  mutate(value = 1)

filter_QH_aug <- QH_aug_long %>%
  spread(QH_aug, value) %>%
  mutate_all(funs(replace_na(.,0))) ## replace na's to 0



## left join filter_QH_aug and df1_spread_aug

final_QH_aug <- merge(filter_QH_aug, df1_spread_aug, by = c("Respondent_Serial"), all = T)


final_QH_aug  <- final_QH_aug  %>%
  mutate_all(funs(replace_na(.,0))) ## replace na's to 0

# x <- colnames(final_QH_aug)
# 
# x <- as.data.frame(x)
# 
# y <- colnames(final_QH_june)
# 
# y <- as.data.frame(y)
# 
# 
# test_test <- x %>%
#   mutate(test = ifelse(x  %in% y$y, 0, 1))


##### merge it with june data since july and june and aug QH have same cols 

final_QH_june <- rbind(final_QH_june, final_QH_july, final_QH_aug)

# QH filter ---------------------------------------------------- Aug above



# QH filter ---------------------------------------------------- May below 

QH <- lto %>%
      select(Respondent_Serial, starts_with("QH"))


## get all the names for QH
question_chains_QH <- do.call(rbind, lapply(QH[, 2:130], function(x) attributes(x)$label)) %>% 
  as.data.frame(stringsAsFactors = FALSE) %>% 
  rownames_to_column() %>% 
  pull(V1)

question_chains_QH <- as.data.frame(question_chains_QH)

question_chains_QH$question_chains_QH <- gsub(':', '', question_chains_QH$question_chains_QH)

question_chains_QH$question_chains_QH <- trimws(question_chains_QH$question_chains_QH)

colnames(QH) <- c("Respondent_Serial",question_chains_QH$question_chains_QH)

QH_long <- QH %>% 
  gather(QH, value, -1) %>%
  drop_na() %>%
  filter(!value %in% c(8,9)) %>%
  mutate(value = 1)

filter_QH <- QH_long %>%
            spread(QH, value) %>%
            mutate_all(funs(replace_na(.,0))) ## replace na's to 0

# QH filter ---------------------------------------------------- May above


## QH filter ---------------------------------------- May and june below

x <- as.data.frame(colnames(final_QH_june))

y <- as.data.frame(colnames(filter_QH))

colnames(x) <- c("x")
colnames(y) <- c("y")


## outer join x and y to find the distincts

## add this this to 
testx <- x %>%
  mutate(test = ifelse(x  %in% y$y, 0, 1)) %>%
  filter(test == 1)

testy <- y %>%
  mutate(test = ifelse(y  %in% x$x, 0, 1)) %>%
  filter(test == 1)

## create cols to add
namevector_may <- testx$x
namevector_june <- testy$y

## change into character
namevector_may <- as.character(namevector_may)
namevector_june <- as.character(namevector_june)


## add cols to make it 134 for may
filter_QH[, namevector_may] <- NA

## add cols to make it 134 for june
final_QH_june[, namevector_june] <- NA



testmay <- as.data.frame(colnames(filter_QH))
  
testjune <- as.data.frame(colnames(final_QH_june))

colnames(testmay) <- c("may")
colnames(testjune) <- c("june")



testmay <- testmay %>%
  filter(may != "Respondent_Serial") %>%
  arrange(may)

testmay$may <- as.character(testmay$may)

testjune <- testjune %>%
  filter(june != "Respondent_Serial") %>%
  arrange(june)

testjune$june <- as.character(testjune$june)



filter_QH2 <- filter_QH %>%
              select(1, testmay$may)


final_QH_june2 <- final_QH_june  %>%
             select(1, testjune$june)

## combine the May and June 
filter_QH <- rbind(filter_QH2, final_QH_june2)

filter_QH <- filter_QH %>%
             mutate_all(funs(replace_na(.,0))) %>%
             distinct()

#filter_QH$Respondent_Serial <- trimws(filter_QH$Respondent_Serial)

## total there are 3998 ID's
test_total_id <- filter_QH %>%
        distinct(Respondent_Serial)


#unique(filter_QH$Respondent_Serial)



## QH filter ---------------------------------------- May and june above

# ## combined all the filters 

demo_house <- merge(demographic_filter, filter_household, by = c("Respondent_Serial"))

id_test_demo <- demo_house %>%
               distinct(Respondent_Serial)

glimpse(demo_house)

glimpse(filter_value_seeker_long)


demo_house_seeker <- merge(demo_house, filter_value_seeker_long, by = c("Respondent_Serial"), all.x = T)


#filter_segment_long

id_test_seg <- filter_segment_long %>%
  distinct(Respondent_Serial)


demo_house_seeker_seg <- merge(demo_house_seeker, filter_segment_long, by = c("Respondent_Serial"), all = T)



# demo_test <- demo_house_seeker_seg %>%
#             select(-segment) %>%
#             distinct()



### now merge demo_house_seeker_seg with QH for final filter

final_filter <- merge(demo_house_seeker_seg, filter_QH, by = c("Respondent_Serial"), all.x = T)




#View(data.frame(names(final_filter)))



final_filter <- final_filter %>%
                mutate_at(16:147, funs(replace_na(.,0))) ## just the QH values



# test <- final_filter %>%
#         distinct(Respondent_Serial)









# hou_seg <- merge(filter_household, filter_segment_long, by = c("Respondent_Serial"))
# 
# 
# 
# 
# hou_seg_QH <- merge(hou_seg, filter_QH, by = c("Respondent_Serial"))
# 
# 
# 
# 
# 
# #filter_value_seeker2 (1199)
# hou_seg_QH_val <- merge(hou_seg_QH, filter_value_seeker_long, by = c("Respondent_Serial"), all.x = T)
# 
# hou_seg_QH_val_demo <- merge(hou_seg_QH_val, demographic_filter, by = c("Respondent_Serial"))

#View(data.frame(names(hou_seg_QH_val_demo)))

# ## replace NA's with 0 mainly for when left join with filter_value_seeker2
# final_filter <- hou_seg_QH_val_demo %>%
#   select(1, 146:153, 143:145, 2:142) %>%
#   mutate_all(funs(replace_na(.,0))) #%>%
#   #mutate_if(is.character, is.factor)

final_filter$Gender <- as.factor(final_filter$Gender)

final_filter$Region <- as.factor(final_filter$Region)

final_filter$Generation <- as.factor(final_filter$Generation)

final_filter$AgeGroup <- as.factor(final_filter$AgeGroup)

final_filter$Income2 <- as.factor(final_filter$Income2)

final_filter$Ethnicity <- as.factor(final_filter$Ethnicity)

final_filter$segment <- as.factor(final_filter$segment)

final_filter$value_seeker <- as.factor(final_filter$value_seeker)

final_filter <- final_filter %>%
  mutate(month = ifelse(Respondent_Serial %in% may_Id, "May", ifelse(Respondent_Serial %in% july_Id, "July", ifelse(Respondent_Serial %in% aug_Id, "August", "June")))) %>%
  select(Respondent_Serial, month, everything())


## change it to factor for R shiny
final_filter$month <- as.factor(final_filter$month)


## 3885 total 15 are missing from here

final_filter_test <- final_filter %>%
                     distinct(Respondent_Serial)


######################################################################################################################

###-----------------------June below Q1

Q1_june <- lto_june %>%
  select(Respondent_Serial, starts_with("Q1"))

#View(data.frame(names(Q1_july)))

#View(data.frame(names(lto_june)))

## get all the names for Q1
question_chains1 <- do.call(rbind, lapply(Q1_june[, 2:123], function(x) attributes(x)$label)) %>% 
  as.data.frame(stringsAsFactors = FALSE) %>% 
  rownames_to_column() %>% 
  pull(V1)

q1_chain_june <- as.data.frame(question_chains1)
glimpse(q1_chain_june)


q1_chain_june2 <- q1_chain_june %>%
  separate(question_chains1, c("x", "y"), "<u>")

q1_chain_june3 <- q1_chain_june2 %>%
  separate(y, c("x2", "y2"), "</u>")


q1_chain_june3$offer <- rm_between(q1_chain_june3$y, "<strong>", "</strong>", extract=TRUE)

q1_chain_june3$y2  <- trimws(q1_chain_june3$y2)

q1_chain_june3$y2 <- substring(q1_chain_june3$y2, 10)

## extract everything after this word </strong>
q1_chain_june3$description <- str_extract(q1_chain_june3$y2, "</strong>.*")

q1_chain_june3$description <- gsub("</strong> :", "", q1_chain_june3$description)

q1_chain_june3$description <- gsub("</strong>", "", q1_chain_june3$description)

q1_chain_june3 <- q1_chain_june3 %>%
  select(-1, -3)

colnames(q1_chain_june3) <- c("brand","offer", "description")

q1_chain_june3$offer <- ifelse(q1_chain_june3$offer == "$29.99 Heat N' Serve Macaroni &amp; Cheese", "$29.99 Heat N’ Serve Macaroni and Cheese", q1_chain_june3$offer)


## trim everything 
q1_chain_june3$description  <- trimws(q1_chain_june3$description)
q1_chain_june3$brand  <- trimws(q1_chain_june3$brand)
q1_chain_june3$offer  <- trimws(q1_chain_june3$offer)


q1_chain_june4 <- q1_chain_june3 %>%
  mutate(Q1 = paste0(brand, "_", offer, description))

#- King Cookies are made using 1 lb.of cookie batter that is fresh, never frozen and baked daily on-site. They are 8&quot; in diameter and can serve 4-	

unique(q1_chain_june4$description)


q1_chain_june4$offer <- ifelse(q1_chain_june4$description == "- King Cookies are made using 1 lb.of cookie batter that is fresh, never frozen and baked daily on-site. They are 8&quot; in diameter and can serve 4-",
                               "Buy Large Pizza get a King Cookie for 5$", q1_chain_june4$offer)

colnames(Q1_june) <- c("Id", q1_chain_june4$Q1)

#names_freq <- as.data.frame(table(names(Q1_june)))
#names_freq[names_freq$Freq > 1, ]
#View(data.frame(names(Q1_june)))


Q1_long_june <- Q1_june %>% 
  gather(Q1, value, -1) %>%
  drop_na()

## now get the offer, description
## left join Q1_long with q1_chain4

Q1_final_june <- merge(Q1_long_june, q1_chain_june4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_june)


Q1_final_june <- Q1_final_june %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)


Q1_final_june$value <- gsub("1", "Not_Likely_At_All", Q1_final_june$value)
Q1_final_june$value <- gsub("2", "Not_very_Likely", Q1_final_june$value)
Q1_final_june$value <- gsub("3", "Neither_Likely_nor_unlikely", Q1_final_june$value)
Q1_final_june$value <- gsub("4", "Likely", Q1_final_june$value)
Q1_final_june$value <- gsub("5", "Very_Likely", Q1_final_june$value)







Q1_final_june$value <- as.factor(Q1_final_june$value)

Q1_final_june$offer2 <- Q1_final_june$offer

Q1_final_june$offer2 <- str_replace_all(Q1_final_june$offer2, "[[:punct:]]", " ")
Q1_final_june$offer2 <- tolower(Q1_final_june$offer2)


Q1_final_june$brand2 <- Q1_final_june$brand


Q1_final_june$brand2 <- str_replace_all(Q1_final_june$brand2, "[[:punct:]]", " ")
Q1_final_june$brand2 <- tolower(Q1_final_june$brand2)

unique(Q1_final_june$offer2)


## i changed the name of offer to make it unique

#Specify sheet by its name
description_map_june <- read_excel("June 2020 Value LTOs.xlsx", sheet = "Sheet1")

description_map_june <- description_map_june %>%
  mutate_if(is.character, str_trim) %>%
  mutate(offer2 = offer)

description_map_june$offer2 <- str_replace_all(description_map_june$offer2, "[[:punct:]]", " ")

description_map_june$offer2 <- tolower(description_map_june$offer2)


Q1_final_june <- Q1_final_june %>%
  mutate_if(is.character, str_trim)

description_map_june <- description_map_june %>%
  mutate_if(is.character, str_trim)

####

# test_Q1_june <- Q1_final_june %>%
#        distinct(offer2)
# 
# 
# test_description_june <- description_map_june %>%
#   distinct(offer2)
# 
# 
# test_june <- merge(test_Q1_june, test_description_june, by = c("offer2"), all = TRUE)


#description_map_june[duplicated(description_map_june$offer2),]


Q1_final_june2 <- merge(Q1_final_june, description_map_june, by = c("offer2"), all.x = TRUE)

glimpse(Q1_final_june2)


Q1_final_june2 <- Q1_final_june2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q1_final_june2)[which(names(Q1_final_june2) == "offer.x")] <- "offer"

colnames(Q1_final_june2)[which(names(Q1_final_june2) == "Company")] <- "chain"

glimpse(Q1_final_june2)

unique(Q1_final_june2$`Month Tested`)

Q1_final_june2$`Month Tested` <- ifelse(Q1_final_june2$`Month Tested` == "Client", "Client", "ALL")


###-----------------------June above Q1



###-----------------------July below Q1

Q1_july <- lto_july %>%
  select(Respondent_Serial, starts_with("Q1"))

#View(data.frame(names(Q1_july)))

#View(data.frame(names(lto_june)))

## get all the names for Q1
question_chains1 <- do.call(rbind, lapply(Q1_july[, 2:102], function(x) attributes(x)$label)) %>% 
  as.data.frame(stringsAsFactors = FALSE) %>% 
  rownames_to_column() %>% 
  pull(V1)

q1_chain_july <- as.data.frame(question_chains1)
glimpse(q1_chain_july)


q1_chain_july2 <- q1_chain_july %>%
  separate(question_chains1, c("x", "y"), "<u>")

q1_chain_july3 <- q1_chain_july2 %>%
  separate(y, c("x2", "y2"), "</u>")


q1_chain_july3$offer <- rm_between(q1_chain_july3$y, "<strong>", "</strong>", extract=TRUE)

q1_chain_july3$y2  <- trimws(q1_chain_july3$y2)

q1_chain_july3$y2 <- substring(q1_chain_july3$y2, 10)

## extract everything after this word </strong>
q1_chain_july3$description <- str_extract(q1_chain_july3$y2, "</strong>.*")

q1_chain_july3$description <- gsub("</strong> :", "", q1_chain_july3$description)

q1_chain_july3$description <- gsub("</strong>", "", q1_chain_july3$description)

q1_chain_july3 <- q1_chain_july3 %>%
  select(-1, -3)

colnames(q1_chain_july3) <- c("brand","offer", "description")

#q1_chain_july3$offer <- ifelse(q1_chain_july3$offer == "$29.99 Heat N' Serve Macaroni &amp; Cheese", "$29.99 Heat N’ Serve Macaroni and Cheese", q1_chain_july3$offer)


## trim everything 
q1_chain_july3$description  <- trimws(q1_chain_july3$description)
q1_chain_july3$brand  <- trimws(q1_chain_july3$brand)
q1_chain_july3$offer  <- trimws(q1_chain_july3$offer)


q1_chain_july4 <- q1_chain_july3 %>%
  mutate(Q1 = paste0(brand, "_", offer, description))

#- King Cookies are made using 1 lb.of cookie batter that is fresh, never frozen and baked daily on-site. They are 8&quot; in diameter and can serve 4-	

unique(q1_chain_july4$description)


# q1_chain_july4$offer <- ifelse(q1_chain_july4$description == "- King Cookies are made using 1 lb.of cookie batter that is fresh, never frozen and baked daily on-site. They are 8&quot; in diameter and can serve 4-",
#                                "Buy Large Pizza get a King Cookie for 5$", q1_chain_july4$offer)

colnames(Q1_july) <- c("Id", q1_chain_july4$Q1)

#names_freq <- as.data.frame(table(names(Q1_july)))
#names_freq[names_freq$Freq > 1, ]
#View(data.frame(names(Q1_july)))


Q1_long_july <- Q1_july %>% 
  gather(Q1, value, -1) %>%
  drop_na()

## now get the offer, description
## left join Q1_long with q1_chain4

Q1_final_july <- merge(Q1_long_july, q1_chain_july4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_july)


Q1_final_july <- Q1_final_july %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)


Q1_final_july$value <- gsub("1", "Not_Likely_At_All", Q1_final_july$value)
Q1_final_july$value <- gsub("2", "Not_very_Likely", Q1_final_july$value)
Q1_final_july$value <- gsub("3", "Neither_Likely_nor_unlikely", Q1_final_july$value)
Q1_final_july$value <- gsub("4", "Likely", Q1_final_july$value)
Q1_final_july$value <- gsub("5", "Very_Likely", Q1_final_july$value)







Q1_final_july$value <- as.factor(Q1_final_july$value)

Q1_final_july$offer2 <- Q1_final_july$offer

Q1_final_july$offer2 <- str_replace_all(Q1_final_july$offer2, "[[:punct:]]", " ")
Q1_final_july$offer2 <- tolower(Q1_final_july$offer2)


Q1_final_july$brand2 <- Q1_final_july$brand


Q1_final_july$brand2 <- str_replace_all(Q1_final_july$brand2, "[[:punct:]]", " ")
Q1_final_july$brand2 <- tolower(Q1_final_july$brand2)

unique(Q1_final_july$offer2)


## i changed the name of offer to make it unique

# #Specify sheet by its name
description_map_july <- read_excel("July 2020 Value LTOs.xlsx", sheet = "Sheet1")

description_map_july <- description_map_july %>%
  mutate_if(is.character, str_trim) %>%
  mutate(offer2 = offer)

description_map_july$offer2 <- str_replace_all(description_map_july$offer2, "[[:punct:]]", " ")

description_map_july$offer2 <- tolower(description_map_july$offer2)


Q1_final_july <- Q1_final_july %>%
  mutate_if(is.character, str_trim)

description_map_july <- description_map_july %>%
  mutate_if(is.character, str_trim)


# test_Q1_july <- Q1_final_july %>%
#        distinct(offer2)
# 
# 
# test_description_july <- description_map_july %>%
#   distinct(offer2)
# 
# 
#test_july <- merge(test_Q1_july, test_description_july, by = c("offer2"), all = TRUE)

#description_map_july[duplicated(description_map_july$offer2),]


Q1_final_july2 <- merge(Q1_final_july, description_map_july, by = c("offer2"), all.x = TRUE)

glimpse(Q1_final_july2)


Q1_final_july2 <- Q1_final_july2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q1_final_july2)[which(names(Q1_final_july2) == "offer.x")] <- "offer"

colnames(Q1_final_july2)[which(names(Q1_final_july2) == "Company")] <- "chain"

glimpse(Q1_final_july2)

unique(Q1_final_july2$`Month Tested`)

Q1_final_july2$`Month Tested` <- as.character(Q1_final_july2$`Month Tested`)

Q1_final_july2$`Month Tested` <- ifelse(Q1_final_july2$`Month Tested` == "Client", "Client", "ALL")


###-----------------------July above Q1



###-----------------------aug below Q1

Q1_aug <- lto_aug %>%
  select(Respondent_Serial, starts_with("Q1"))

#View(data.frame(names(Q1_aug)))

#View(data.frame(names(lto_june)))

## get all the names for Q1
question_chains1 <- do.call(rbind, lapply(Q1_aug[, 2:97], function(x) attributes(x)$label)) %>% 
  as.data.frame(stringsAsFactors = FALSE) %>% 
  rownames_to_column() %>% 
  pull(V1)

q1_chain_aug <- as.data.frame(question_chains1)
glimpse(q1_chain_aug)


q1_chain_aug2 <- q1_chain_aug %>%
  separate(question_chains1, c("x", "y"), "<u>")

q1_chain_aug3 <- q1_chain_aug2 %>%
  separate(y, c("x2", "y2"), "</u>")


q1_chain_aug3$offer <- rm_between(q1_chain_aug3$y, "<strong>", "</strong>", extract=TRUE)


q1_chain_aug3$offer <- gsub("&amp;", "", q1_chain_aug3$offer)


q1_chain_aug3$y2  <- trimws(q1_chain_aug3$y2)

q1_chain_aug3$y2 <- substring(q1_chain_aug3$y2, 10)

## extract everything after this word </strong>
q1_chain_aug3$description <- str_extract(q1_chain_aug3$y2, "</strong>.*")

q1_chain_aug3$description <- gsub("</strong> :", "", q1_chain_aug3$description)

q1_chain_aug3$description <- gsub("</strong>", "", q1_chain_aug3$description)

q1_chain_aug3 <- q1_chain_aug3 %>%
  select(-1, -3)


#<strong>On The Border Mexican Grill &amp; Cantina</strong>


q1_chain_aug3$x2 <- ifelse(q1_chain_aug3$x2 == "<strong>On The Border Mexican Grill &amp; Cantina</strong>", "On The Border Mexican Grill & Cantina", q1_chain_aug3$x2)

q1_chain_aug3$x2 <- ifelse(q1_chain_aug3$x2 == "On The Border Mexican Grill &amp; Cantina", "On The Border Mexican Grill & Cantina", q1_chain_aug3$x2)


colnames(q1_chain_aug3) <- c("brand","offer", "description")

q1_chain_aug3$offer <- ifelse(q1_chain_aug3$offer == "$29.99 Heat N' Serve Macaroni &amp; Cheese", "$29.99 Heat N’ Serve Macaroni and Cheese", q1_chain_aug3$offer)


## trim everything 
q1_chain_aug3$description  <- trimws(q1_chain_aug3$description)
q1_chain_aug3$brand  <- trimws(q1_chain_aug3$brand)
q1_chain_aug3$offer  <- trimws(q1_chain_aug3$offer)


q1_chain_aug4 <- q1_chain_aug3 %>%
  mutate(Q1 = paste0(brand, "_", offer, description))

#- King Cookies are made using 1 lb.of cookie batter that is fresh, never frozen and baked daily on-site. They are 8&quot; in diameter and can serve 4-	

unique(q1_chain_aug4$description)


q1_chain_aug4$offer <- ifelse(q1_chain_aug4$description == "- King Cookies are made using 1 lb.of cookie batter that is fresh, never frozen and baked daily on-site. They are 8&quot; in diameter and can serve 4-",
                               "Buy Large Pizza get a King Cookie for 5$", q1_chain_aug4$offer)

colnames(Q1_aug) <- c("Id", q1_chain_aug4$Q1)

#names_freq <- as.data.frame(table(names(Q1_aug)))
#names_freq[names_freq$Freq > 1, ]
#View(data.frame(names(Q1_aug)))


Q1_long_aug <- Q1_aug %>% 
  gather(Q1, value, -1) %>%
  drop_na()

## now get the offer, description
## left join Q1_long with q1_chain4

Q1_final_aug <- merge(Q1_long_aug, q1_chain_aug4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_aug)

## remove space between the sentence
Q1_final_aug$offer2 <- gsub('\\s+', '', Q1_final_aug$offer2)



Q1_final_aug <- Q1_final_aug %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)


Q1_final_aug$value <- gsub("1", "Not_Likely_At_All", Q1_final_aug$value)
Q1_final_aug$value <- gsub("2", "Not_very_Likely", Q1_final_aug$value)
Q1_final_aug$value <- gsub("3", "Neither_Likely_nor_unlikely", Q1_final_aug$value)
Q1_final_aug$value <- gsub("4", "Likely", Q1_final_aug$value)
Q1_final_aug$value <- gsub("5", "Very_Likely", Q1_final_aug$value)







Q1_final_aug$value <- as.factor(Q1_final_aug$value)

Q1_final_aug$offer2 <- Q1_final_aug$offer

Q1_final_aug$offer2 <- str_replace_all(Q1_final_aug$offer2, "[[:punct:]]", " ")
Q1_final_aug$offer2 <- tolower(Q1_final_aug$offer2)


Q1_final_aug$brand2 <- Q1_final_aug$brand


Q1_final_aug$brand2 <- str_replace_all(Q1_final_aug$brand2, "[[:punct:]]", " ")
Q1_final_aug$brand2 <- tolower(Q1_final_aug$brand2)

unique(Q1_final_aug$offer2)

Q1_final_aug$offer2 <- ifelse(Q1_final_aug$brand == "Checkers Drive-In Restaurants", "1$ Famous Seasoned Fries", Q1_final_aug$offer2)

## remove space between the sentence
Q1_final_aug$offer2 <- gsub('\\s+', '', Q1_final_aug$offer2)


Q1_final_aug <- Q1_final_aug %>%
  mutate_if(is.character, str_trim)


test_Q1_final_aug <- Q1_final_aug %>%
                    distinct(offer2)

unique(Q1_final_aug$offer2)


#Q1_final_aug[duplicated(Q1_final_aug$offer2),]




## i changed the name of offer to make it unique

# #Specify sheet by its name
description_map_aug <- read_excel("August 2020 Value LTOs.xlsx", sheet = "Sheet1")

description_map_aug <- description_map_aug %>%
  mutate_if(is.character, str_trim) %>%
  mutate(offer2 = offer)

description_map_aug$offer2 <- str_replace_all(description_map_aug$offer2, "[[:punct:]]", " ")

description_map_aug$offer2 <- tolower(description_map_aug$offer2)


description_map_aug$offer2 <- ifelse(description_map_aug$Company == "Checkers Drive-In Restaurants", "1$ Famous Seasoned Fries", description_map_aug$offer2)

description_map_aug <- description_map_aug %>%
  mutate_if(is.character, str_trim)


## remove space between the sentence
description_map_aug$offer2 <- gsub('\\s+', '', description_map_aug$offer2)

#unique(Q1_final_aug$Q1)
#unique(description_map_aug$offer2)


#description_map_aug[duplicated(description_map_aug$offer2),]
#Q1_final_aug[duplicated(Q1_final_aug$offer2),]

## make the offer2 unique so you can test it after it should be 96


test_Q1_aug <- Q1_final_aug %>%
       distinct(offer2)


test_description_aug <- description_map_aug %>%
  distinct(offer2)


test_aug <- merge(test_Q1_aug, test_description_aug, by = c("offer2"), all = TRUE)










Q1_final_aug2 <- merge(Q1_final_aug, description_map_aug, by = c("offer2"), all.x = TRUE)



glimpse(Q1_final_aug2)


Q1_final_aug2 <- Q1_final_aug2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q1_final_aug2)[which(names(Q1_final_aug2) == "offer.x")] <- "offer"

colnames(Q1_final_aug2)[which(names(Q1_final_aug2) == "Company")] <- "chain"

glimpse(Q1_final_aug2)

unique(Q1_final_aug2$`Month Tested`)

Q1_final_aug2$`Month Tested` <- as.character(Q1_final_aug2$`Month Tested`)

Q1_final_aug2$`Month Tested` <- ifelse(Q1_final_aug2$`Month Tested` == "Client", "Client", "ALL")


###-----------------------aug above Q1

Q1 <- lto %>%
      select(Respondent_Serial, starts_with("Q1"))


## get all the names for Q1
question_chains1 <- do.call(rbind, lapply(Q1[, 2:144], function(x) attributes(x)$label)) %>% 
  as.data.frame(stringsAsFactors = FALSE) %>% 
  rownames_to_column() %>% 
  pull(V1)

q1_chain <- as.data.frame(question_chains1)
glimpse(q1_chain)


q1_chain2 <- q1_chain %>%
  separate(question_chains1, c("x", "y"), "<u>")

q1_chain3 <- q1_chain2 %>%
  separate(y, c("x2", "y2"), "</u>")



q1_chain3$offer <- rm_between(q1_chain3$y, "<strong>", "</strong>", extract=TRUE)

q1_chain3$y2  <- trimws(q1_chain3$y2)
              
q1_chain3$y2 <- substring(q1_chain3$y2, 10)

## extract everything after this word </strong>
q1_chain3$description <- str_extract(q1_chain3$y2, "</strong>.*")

q1_chain3$description <- gsub("</strong> :", "", q1_chain3$description)

q1_chain3$description <- gsub("</strong>", "", q1_chain3$description)

q1_chain3 <- q1_chain3 %>%
             select(-1, -3)

colnames(q1_chain3) <- c("brand","offer", "description")

q1_chain3$offer <- ifelse(q1_chain3$offer == "$29.99 Heat N' Serve Macaroni &amp; Cheese", "$29.99 Heat N’ Serve Macaroni and Cheese", q1_chain3$offer)


## trim everything 
q1_chain3$description  <- trimws(q1_chain3$description)
q1_chain3$brand  <- trimws(q1_chain3$brand)
q1_chain3$offer  <- trimws(q1_chain3$offer)


q1_chain4 <- q1_chain3 %>%
             mutate(Q1 = paste0(brand, "_", offer))

colnames(Q1) <- c("Id", q1_chain4$Q1)


## check for duplicates
#q1_chain4$questions[duplicated(q1_chain4$questions)]

unique(q1_chain4$Q1)

Q1_long <- Q1 %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q1_long)

## now get the offer, description
## left join Q1_long with q1_chain4

Q1_final <- merge(Q1_long, q1_chain4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final)


Q1_final <- Q1_final %>%
             #select(-1) %>%
             mutate_if(is.character, str_trim)


Q1_final$value <- gsub("1", "Not_Likely_At_All", Q1_final$value)
Q1_final$value <- gsub("2", "Not_very_Likely", Q1_final$value)
Q1_final$value <- gsub("3", "Neither_Likely_nor_unlikely", Q1_final$value)
Q1_final$value <- gsub("4", "Likely", Q1_final$value)
Q1_final$value <- gsub("5", "Very_Likely", Q1_final$value)

Q1_final$value <- as.factor(Q1_final$value)

Q1_final$offer2 <- Q1_final$offer

Q1_final$offer2 <- str_replace_all(Q1_final$offer2, "[[:punct:]]", " ")
Q1_final$offer2 <- tolower(Q1_final$offer2)


Q1_final$brand2 <- Q1_final$brand


Q1_final$brand2 <- str_replace_all(Q1_final$brand2, "[[:punct:]]", " ")
Q1_final$brand2 <- tolower(Q1_final$brand2)


#Specify sheet by its name
description_map <- read_excel("May 2020 Value LTOs_ v2.xlsx", sheet = "Sheet1")

description_map <- description_map %>%
  mutate_if(is.character, str_trim) %>%
  mutate(offer2 = offer)

glimpse(Q1_final)
glimpse(description_map)

description_map$offer2 <- str_replace_all(description_map$offer2, "[[:punct:]]", " ")

description_map$offer2 <- tolower(description_map$offer2)


Q1_final <- Q1_final %>%
  mutate_if(is.character, str_trim)

description_map <- description_map %>%
  mutate_if(is.character, str_trim)

#description_map[duplicated(description_map$offer2),]


Q1_final2 <- merge(Q1_final, description_map, by = c("offer2"), all.x = TRUE)

glimpse(Q1_final2)

Q1_final2 <- Q1_final2 %>%
             select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
             mutate_if(is.character, as.factor)
  

colnames(Q1_final2)[which(names(Q1_final2) == "offer.x")] <- "offer"

colnames(Q1_final2)[which(names(Q1_final2) == "Company")] <- "chain"


glimpse(Q1_final2)


Q1_final2$`Month Tested` <- ifelse(Q1_final2$`Month Tested` == "Client", "Client", "ALL")



########################################### Q2 june below

Q2_june <- lto_june %>%
  select(Respondent_Serial, starts_with("Q2"))


colnames(Q2_june) <- c("Id", q1_chain_june4$Q1)

Q2_long_june <- Q2_june %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q2_long_june)

## now get the offer, description
## left join Q1_long with q1_chain4

Q2_final_june <- merge(Q2_long_june, q1_chain_june4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_june)


Q2_final_june <- Q2_final_june %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q2_final_june$value <- gsub("1", "Not_good_value_at_all", Q2_final_june$value)
Q2_final_june$value <- gsub("2", "Not_that_good_value", Q2_final_june$value)
Q2_final_june$value <- gsub("3", "Unsure_value", Q2_final_june$value)
Q2_final_june$value <- gsub("4", "good_value", Q2_final_june$value)
Q2_final_june$value <- gsub("5", "very_good_value", Q2_final_june$value)

Q2_final_june$value <- as.factor(Q2_final_june$value)

Q2_final_june$offer2 <- Q2_final_june$offer

Q2_final_june$offer2 <- str_replace_all(Q2_final_june$offer2, "[[:punct:]]", " ")
Q2_final_june$offer2 <- tolower(Q2_final_june$offer2)


Q2_final_june$brand2 <- Q2_final_june$brand


Q2_final_june$brand2 <- str_replace_all(Q2_final_june$brand2, "[[:punct:]]", " ")
Q2_final_june$brand2 <- tolower(Q2_final_june$brand2)

Q2_final_june <- Q2_final_june %>%
  mutate_if(is.character, str_trim)


Q2_final_june2 <- merge(Q2_final_june, description_map_june, by = c("offer2"), all.x = TRUE)

glimpse(Q2_final_june2)

Q2_final_june2 <- Q2_final_june2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q2_final_june2)[which(names(Q2_final_june2) == "offer.x")] <- "offer"

colnames(Q2_final_june2)[which(names(Q2_final_june2) == "Company")] <- "chain"

Q2_final_june2$`Month Tested` <- ifelse(Q2_final_june2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q2 june above 



########################################### Q2 july below

Q2_july <- lto_july %>%
  select(Respondent_Serial, starts_with("Q2"))


colnames(Q2_july) <- c("Id", q1_chain_july4$Q1)

Q2_long_july <- Q2_july %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q2_long_july)

## now get the offer, description
## left join Q1_long with q1_chain4

Q2_final_july <- merge(Q2_long_july, q1_chain_july4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_july)


Q2_final_july <- Q2_final_july %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q2_final_july$value <- gsub("1", "Not_good_value_at_all", Q2_final_july$value)
Q2_final_july$value <- gsub("2", "Not_that_good_value", Q2_final_july$value)
Q2_final_july$value <- gsub("3", "Unsure_value", Q2_final_july$value)
Q2_final_july$value <- gsub("4", "good_value", Q2_final_july$value)
Q2_final_july$value <- gsub("5", "very_good_value", Q2_final_july$value)

Q2_final_july$value <- as.factor(Q2_final_july$value)

Q2_final_july$offer2 <- Q2_final_july$offer

Q2_final_july$offer2 <- str_replace_all(Q2_final_july$offer2, "[[:punct:]]", " ")
Q2_final_july$offer2 <- tolower(Q2_final_july$offer2)


Q2_final_july$brand2 <- Q2_final_july$brand


Q2_final_july$brand2 <- str_replace_all(Q2_final_july$brand2, "[[:punct:]]", " ")
Q2_final_july$brand2 <- tolower(Q2_final_july$brand2)

Q2_final_july <- Q2_final_july %>%
  mutate_if(is.character, str_trim)


Q2_final_july2 <- merge(Q2_final_july, description_map_july, by = c("offer2"), all.x = TRUE)

glimpse(Q2_final_july2)

Q2_final_july2 <- Q2_final_july2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q2_final_july2)[which(names(Q2_final_july2) == "offer.x")] <- "offer"

colnames(Q2_final_july2)[which(names(Q2_final_july2) == "Company")] <- "chain"

Q2_final_july2$`Month Tested` <- as.character(Q2_final_july2$`Month Tested`)


Q2_final_july2$`Month Tested` <- ifelse(Q2_final_july2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q2 july above 




########################################### Q2 aug below

Q2_aug <- lto_aug %>%
  select(Respondent_Serial, starts_with("Q2"))


colnames(Q2_aug) <- c("Id", q1_chain_aug4$Q1)

Q2_long_aug <- Q2_aug %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q2_long_aug)

## now get the offer, description
## left join Q1_long with q1_chain4

Q2_final_aug <- merge(Q2_long_aug, q1_chain_aug4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_aug)


Q2_final_aug <- Q2_final_aug %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q2_final_aug$value <- gsub("1", "Not_good_value_at_all", Q2_final_aug$value)
Q2_final_aug$value <- gsub("2", "Not_that_good_value", Q2_final_aug$value)
Q2_final_aug$value <- gsub("3", "Unsure_value", Q2_final_aug$value)
Q2_final_aug$value <- gsub("4", "good_value", Q2_final_aug$value)
Q2_final_aug$value <- gsub("5", "very_good_value", Q2_final_aug$value)

Q2_final_aug$value <- as.factor(Q2_final_aug$value)

Q2_final_aug$offer2 <- Q2_final_aug$offer

Q2_final_aug$offer2 <- str_replace_all(Q2_final_aug$offer2, "[[:punct:]]", " ")
Q2_final_aug$offer2 <- tolower(Q2_final_aug$offer2)


Q2_final_aug$brand2 <- Q2_final_aug$brand


Q2_final_aug$brand2 <- str_replace_all(Q2_final_aug$brand2, "[[:punct:]]", " ")
Q2_final_aug$brand2 <- tolower(Q2_final_aug$brand2)

## make changes here
Q2_final_aug$offer2 <- ifelse(Q2_final_aug$brand == "Checkers Drive-In Restaurants", "1$ Famous Seasoned Fries", Q2_final_aug$offer2)


## remove space between the sentence
Q2_final_aug$offer2 <- gsub('\\s+', '', Q2_final_aug$offer2)

Q2_final_aug <- Q2_final_aug %>%
  mutate_if(is.character, str_trim)


# test_Q2_aug <- Q2_final_aug %>%
#        distinct(offer2)
# 
# 
# test_description_aug <- description_map_aug %>%
#   distinct(offer2)
# 
# 
# test_aug <- merge(test_Q2_aug, test_description_aug, by = c("offer2"), all.x = TRUE)





Q2_final_aug2 <- merge(Q2_final_aug, description_map_aug, by = c("offer2"), all.x = TRUE)

glimpse(Q2_final_aug2)

Q2_final_aug2 <- Q2_final_aug2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q2_final_aug2)[which(names(Q2_final_aug2) == "offer.x")] <- "offer"

colnames(Q2_final_aug2)[which(names(Q2_final_aug2) == "Company")] <- "chain"

Q2_final_aug2$`Month Tested` <- as.character(Q2_final_aug2$`Month Tested`)


Q2_final_aug2$`Month Tested` <- ifelse(Q2_final_aug2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q2 aug above 



#############################################################################################################

Q2 <- lto %>%
  select(Respondent_Serial, starts_with("Q2")) #%>%
  #select_if(is.numeric)

colnames(Q2) <- c("Id", q1_chain4$Q1)

unique(q1_chain4$Q2)

Q2_long <- Q2 %>%
  gather(Q1, value, -1) %>%
  drop_na()

Q2_final <- merge(Q2_long, q1_chain4, by = c("Q1"), all.x = TRUE)

Q2_final <- Q2_final %>%
  select(-1) %>%
  mutate_if(is.character, str_trim)

Q2_final$value <- gsub("1", "Not_good_value_at_all", Q2_final$value)
Q2_final$value <- gsub("2", "Not_that_good_value", Q2_final$value)
Q2_final$value <- gsub("3", "Unsure_value", Q2_final$value)
Q2_final$value <- gsub("4", "good_value", Q2_final$value)
Q2_final$value <- gsub("5", "very_good_value", Q2_final$value)

Q2_final$value <- as.factor(Q2_final$value)

glimpse(Q2_final)


Q2_final$offer2 <- Q2_final$offer

Q2_final$offer2 <- str_replace_all(Q2_final$offer2, "[[:punct:]]", " ")
Q2_final$offer2 <- tolower(Q2_final$offer2)


Q2_final$brand2 <- Q2_final$brand


Q2_final$brand2 <- str_replace_all(Q2_final$brand2, "[[:punct:]]", " ")
Q2_final$brand2 <- tolower(Q2_final$brand2)


glimpse(Q1_final2)

Q2_final <- Q2_final %>%
  mutate_if(is.character, str_trim)



Q2_final2 <- merge(Q2_final, description_map, by = c("offer2"), all.x = TRUE)

Q2_final2 <- Q2_final2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q2_final2)[which(names(Q2_final2) == "offer.x")] <- "offer"

colnames(Q2_final2)[which(names(Q2_final2) == "Company")] <- "chain"

Q2_final2$`Month Tested` <- ifelse(Q2_final2$`Month Tested` == "Client", "Client", "ALL")

###################################################################################################


########################################### Q3 june below

Q3_june <- lto_june %>%
  select(Respondent_Serial, 660:781)


colnames(Q3_june) <- c("Id", q1_chain_june4$Q1)

Q3_long_june <- Q3_june %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q3_long_june)

## now get the offer, description
## left join Q1_long with q1_chain4

Q3_final_june <- merge(Q3_long_june, q1_chain_june4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_june)


Q3_final_june <- Q3_final_june %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q3_final_june$value <- gsub("1", "Portion_size_Seems_good_bang_buck", Q3_final_june$value)
Q3_final_june$value <- gsub("2", "Seems_tasty", Q3_final_june$value)
Q3_final_june$value <- gsub("3", "Seems_high_quality", Q3_final_june$value)
Q3_final_june$value <- gsub("4", "Fits_my_budget", Q3_final_june$value)
Q3_final_june$value <- gsub("5", "Other", Q3_final_june$value)

Q3_final_june$value <- as.factor(Q3_final_june$value)

Q3_final_june$offer2 <- Q3_final_june$offer

Q3_final_june$offer2 <- str_replace_all(Q3_final_june$offer2, "[[:punct:]]", " ")
Q3_final_june$offer2 <- tolower(Q3_final_june$offer2)


Q3_final_june$brand2 <- Q3_final_june$brand


Q3_final_june$brand2 <- str_replace_all(Q3_final_june$brand2, "[[:punct:]]", " ")
Q3_final_june$brand2 <- tolower(Q3_final_june$brand2)


Q3_final_june <- Q3_final_june %>%
  mutate_if(is.character, str_trim)

Q3_final_june2 <- merge(Q3_final_june, description_map_june, by = c("offer2"), all.x = TRUE)

glimpse(Q3_final_june2)

Q3_final_june2 <- Q3_final_june2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q3_final_june2)[which(names(Q3_final_june2) == "offer.x")] <- "offer"

colnames(Q3_final_june2)[which(names(Q3_final_june2) == "Company")] <- "chain"

Q3_final_june2$`Month Tested` <- ifelse(Q3_final_june2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q3 june above 



########################################### Q3 july below

Q3_july <- lto_july %>%
  select(Respondent_Serial, 719:819)


colnames(Q3_july) <- c("Id", q1_chain_july4$Q1)

Q3_long_july <- Q3_july %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q3_long_july)

## now get the offer, description
## left join Q1_long with q1_chain4

Q3_final_july <- merge(Q3_long_july, q1_chain_july4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_july)


Q3_final_july <- Q3_final_july %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q3_final_july$value <- gsub("1", "Portion_size_Seems_good_bang_buck", Q3_final_july$value)
Q3_final_july$value <- gsub("2", "Seems_tasty", Q3_final_july$value)
Q3_final_july$value <- gsub("3", "Seems_high_quality", Q3_final_july$value)
Q3_final_july$value <- gsub("4", "Fits_my_budget", Q3_final_july$value)
Q3_final_july$value <- gsub("5", "Other", Q3_final_july$value)

Q3_final_july$value <- as.factor(Q3_final_july$value)

Q3_final_july$offer2 <- Q3_final_july$offer

Q3_final_july$offer2 <- str_replace_all(Q3_final_july$offer2, "[[:punct:]]", " ")
Q3_final_july$offer2 <- tolower(Q3_final_july$offer2)


Q3_final_july$brand2 <- Q3_final_july$brand


Q3_final_july$brand2 <- str_replace_all(Q3_final_july$brand2, "[[:punct:]]", " ")
Q3_final_july$brand2 <- tolower(Q3_final_july$brand2)


Q3_final_july <- Q3_final_july %>%
  mutate_if(is.character, str_trim)

Q3_final_july2 <- merge(Q3_final_july, description_map_july, by = c("offer2"), all.x = TRUE)

glimpse(Q3_final_july2)

Q3_final_july2 <- Q3_final_july2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q3_final_july2)[which(names(Q3_final_july2) == "offer.x")] <- "offer"

colnames(Q3_final_july2)[which(names(Q3_final_july2) == "Company")] <- "chain"

Q3_final_july2$`Month Tested` <- as.character(Q3_final_july2$`Month Tested`)

Q3_final_july2$`Month Tested` <- ifelse(Q3_final_july2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q3 july above


########################################### Q3 aug below

Q3_aug <- lto_aug %>%
  select(Respondent_Serial, 596:691)


colnames(Q3_aug) <- c("Id", q1_chain_aug4$Q1)

Q3_long_aug <- Q3_aug %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q3_long_aug)

## now get the offer, description
## left join Q1_long with q1_chain4

Q3_final_aug <- merge(Q3_long_aug, q1_chain_aug4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_aug)


Q3_final_aug <- Q3_final_aug %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q3_final_aug$value <- gsub("1", "Portion_size_Seems_good_bang_buck", Q3_final_aug$value)
Q3_final_aug$value <- gsub("2", "Seems_tasty", Q3_final_aug$value)
Q3_final_aug$value <- gsub("3", "Seems_high_quality", Q3_final_aug$value)
Q3_final_aug$value <- gsub("4", "Fits_my_budget", Q3_final_aug$value)
Q3_final_aug$value <- gsub("5", "Other", Q3_final_aug$value)

Q3_final_aug$value <- as.factor(Q3_final_aug$value)

Q3_final_aug$offer2 <- Q3_final_aug$offer

Q3_final_aug$offer2 <- str_replace_all(Q3_final_aug$offer2, "[[:punct:]]", " ")
Q3_final_aug$offer2 <- tolower(Q3_final_aug$offer2)


Q3_final_aug$brand2 <- Q3_final_aug$brand


Q3_final_aug$brand2 <- str_replace_all(Q3_final_aug$brand2, "[[:punct:]]", " ")
Q3_final_aug$brand2 <- tolower(Q3_final_aug$brand2)

## make changes here
Q3_final_aug$offer2 <- ifelse(Q3_final_aug$brand == "Checkers Drive-In Restaurants", "1$ Famous Seasoned Fries", Q3_final_aug$offer2)


## remove space between the sentence
Q3_final_aug$offer2 <- gsub('\\s+', '', Q3_final_aug$offer2)


Q3_final_aug <- Q3_final_aug %>%
  mutate_if(is.character, str_trim)

Q3_final_aug2 <- merge(Q3_final_aug, description_map_aug, by = c("offer2"), all.x = TRUE)

glimpse(Q3_final_aug2)

Q3_final_aug2 <- Q3_final_aug2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q3_final_aug2)[which(names(Q3_final_aug2) == "offer.x")] <- "offer"

colnames(Q3_final_aug2)[which(names(Q3_final_aug2) == "Company")] <- "chain"

Q3_final_aug2$`Month Tested` <- as.character(Q3_final_aug2$`Month Tested`)

Q3_final_aug2$`Month Tested` <- ifelse(Q3_final_aug2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q3 aug above

Q3 <- lto %>%
  select(Respondent_Serial, 681:823) #%>%
#select_if(is.numeric)

colnames(Q3) <- c("Id", q1_chain4$Q1)

unique(q1_chain4$Q3)

Q3_long <- Q3 %>%
  gather(Q1, value, -1) %>%
  drop_na()

Q3_final <- merge(Q3_long, q1_chain4, by = c("Q1"), all.x = TRUE)

Q3_final <- Q3_final %>%
  select(-1) %>%
  mutate_if(is.character, str_trim)

Q3_final$value <- gsub("1", "Portion_size_Seems_good_bang_buck", Q3_final$value)
Q3_final$value <- gsub("2", "Seems_tasty", Q3_final$value)
Q3_final$value <- gsub("3", "Seems_high_quality", Q3_final$value)
Q3_final$value <- gsub("4", "Fits_my_budget", Q3_final$value)
Q3_final$value <- gsub("5", "Other", Q3_final$value)

Q3_final$value <- as.factor(Q3_final$value)

glimpse(Q3_final)


Q3_final$offer2 <- Q3_final$offer

Q3_final$offer2 <- str_replace_all(Q3_final$offer2, "[[:punct:]]", " ")
Q3_final$offer2 <- tolower(Q3_final$offer2)


Q3_final$brand2 <- Q3_final$brand


Q3_final$brand2 <- str_replace_all(Q3_final$brand2, "[[:punct:]]", " ")
Q3_final$brand2 <- tolower(Q3_final$brand2)


glimpse(Q1_final2)

Q3_final <- Q3_final %>%
  mutate_if(is.character, str_trim)

Q3_final2 <- merge(Q3_final, description_map, by = c("offer2"), all.x = TRUE)

Q3_final2 <- Q3_final2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q3_final2)[which(names(Q3_final2) == "offer.x")] <- "offer"

colnames(Q3_final2)[which(names(Q3_final2) == "Company")] <- "chain"

Q3_final2$`Month Tested` <- ifelse(Q3_final2$`Month Tested` == "Client", "Client", "ALL")


########################################### Q4 june below

Q4_june <- lto_june %>%
  select(Respondent_Serial, 904:1025)


colnames(Q4_june) <- c("Id", q1_chain_june4$Q1)

Q4_long_june <- Q4_june %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q4_long_june)

## now get the offer, description
## left join Q1_long with q1_chain4

Q4_final_june <- merge(Q4_long_june, q1_chain_june4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_june)


Q4_final_june <- Q4_final_june %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q4_final_june$value <- gsub("1", "Poor_quality", Q4_final_june$value)
Q4_final_june$value <- gsub("2", "Somewhat_low_quality", Q4_final_june$value)
Q4_final_june$value <- gsub("3", "Standard_quality", Q4_final_june$value)
Q4_final_june$value <- gsub("4", "Good_quality", Q4_final_june$value)
Q4_final_june$value <- gsub("5", "High_quality", Q4_final_june$value)

Q4_final_june$value <- as.factor(Q4_final_june$value)

Q4_final_june$offer2 <- Q4_final_june$offer

Q4_final_june$offer2 <- str_replace_all(Q4_final_june$offer2, "[[:punct:]]", " ")
Q4_final_june$offer2 <- tolower(Q4_final_june$offer2)


Q4_final_june$brand2 <- Q4_final_june$brand


Q4_final_june$brand2 <- str_replace_all(Q4_final_june$brand2, "[[:punct:]]", " ")
Q4_final_june$brand2 <- tolower(Q4_final_june$brand2)

Q4_final_june <- Q4_final_june %>%
  mutate_if(is.character, str_trim)


Q4_final_june2 <- merge(Q4_final_june, description_map_june, by = c("offer2"), all.x = TRUE)

glimpse(Q4_final_june2)

Q4_final_june2 <- Q4_final_june2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q4_final_june2)[which(names(Q4_final_june2) == "offer.x")] <- "offer"

colnames(Q4_final_june2)[which(names(Q4_final_june2) == "Company")] <- "chain"

Q4_final_june2$`Month Tested` <- ifelse(Q4_final_june2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q4 june above 



########################################### Q4 july below

Q4_july <- lto_july %>%
  select(Respondent_Serial, 921:1021)


colnames(Q4_july) <- c("Id", q1_chain_july4$Q1)

Q4_long_july <- Q4_july %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q4_long_july)

## now get the offer, description
## left join Q1_long with q1_chain4

Q4_final_july <- merge(Q4_long_july, q1_chain_july4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_july)


Q4_final_july <- Q4_final_july %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q4_final_july$value <- gsub("1", "Poor_quality", Q4_final_july$value)
Q4_final_july$value <- gsub("2", "Somewhat_low_quality", Q4_final_july$value)
Q4_final_july$value <- gsub("3", "Standard_quality", Q4_final_july$value)
Q4_final_july$value <- gsub("4", "Good_quality", Q4_final_july$value)
Q4_final_july$value <- gsub("5", "High_quality", Q4_final_july$value)

Q4_final_july$value <- as.factor(Q4_final_july$value)

Q4_final_july$offer2 <- Q4_final_july$offer

Q4_final_july$offer2 <- str_replace_all(Q4_final_july$offer2, "[[:punct:]]", " ")
Q4_final_july$offer2 <- tolower(Q4_final_july$offer2)


Q4_final_july$brand2 <- Q4_final_july$brand


Q4_final_july$brand2 <- str_replace_all(Q4_final_july$brand2, "[[:punct:]]", " ")
Q4_final_july$brand2 <- tolower(Q4_final_july$brand2)

Q4_final_july <- Q4_final_july %>%
  mutate_if(is.character, str_trim)


Q4_final_july2 <- merge(Q4_final_july, description_map_july, by = c("offer2"), all.x = TRUE)

glimpse(Q4_final_july2)

Q4_final_july2 <- Q4_final_july2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q4_final_july2)[which(names(Q4_final_july2) == "offer.x")] <- "offer"

colnames(Q4_final_july2)[which(names(Q4_final_july2) == "Company")] <- "chain"


Q4_final_july2$`Month Tested` <- as.character(Q4_final_july2$`Month Tested`)


Q4_final_july2$`Month Tested` <- ifelse(Q4_final_july2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q4 july above


########################################### Q4 aug below

Q4_aug <- lto_aug %>%
  select(Respondent_Serial, 788:883)


colnames(Q4_aug) <- c("Id", q1_chain_aug4$Q1)

Q4_long_aug <- Q4_aug %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q4_long_aug)

## now get the offer, description
## left join Q1_long with q1_chain4

Q4_final_aug <- merge(Q4_long_aug, q1_chain_aug4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_aug)


Q4_final_aug <- Q4_final_aug %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q4_final_aug$value <- gsub("1", "Poor_quality", Q4_final_aug$value)
Q4_final_aug$value <- gsub("2", "Somewhat_low_quality", Q4_final_aug$value)
Q4_final_aug$value <- gsub("3", "Standard_quality", Q4_final_aug$value)
Q4_final_aug$value <- gsub("4", "Good_quality", Q4_final_aug$value)
Q4_final_aug$value <- gsub("5", "High_quality", Q4_final_aug$value)

Q4_final_aug$value <- as.factor(Q4_final_aug$value)

Q4_final_aug$offer2 <- Q4_final_aug$offer

Q4_final_aug$offer2 <- str_replace_all(Q4_final_aug$offer2, "[[:punct:]]", " ")
Q4_final_aug$offer2 <- tolower(Q4_final_aug$offer2)


Q4_final_aug$brand2 <- Q4_final_aug$brand


Q4_final_aug$brand2 <- str_replace_all(Q4_final_aug$brand2, "[[:punct:]]", " ")
Q4_final_aug$brand2 <- tolower(Q4_final_aug$brand2)


## make changes here
Q4_final_aug$offer2 <- ifelse(Q4_final_aug$brand == "Checkers Drive-In Restaurants", "1$ Famous Seasoned Fries", Q4_final_aug$offer2)


## remove space between the sentence
Q4_final_aug$offer2 <- gsub('\\s+', '', Q4_final_aug$offer2)




Q4_final_aug <- Q4_final_aug %>%
  mutate_if(is.character, str_trim)


Q4_final_aug2 <- merge(Q4_final_aug, description_map_aug, by = c("offer2"), all.x = TRUE)

glimpse(Q4_final_aug2)

Q4_final_aug2 <- Q4_final_aug2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q4_final_aug2)[which(names(Q4_final_aug2) == "offer.x")] <- "offer"

colnames(Q4_final_aug2)[which(names(Q4_final_aug2) == "Company")] <- "chain"


Q4_final_aug2$`Month Tested` <- as.character(Q4_final_aug2$`Month Tested`)


Q4_final_aug2$`Month Tested` <- ifelse(Q4_final_aug2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q4 aug above









Q4 <- lto %>%
  select(Respondent_Serial, 967:1109) #%>%
#select_if(is.numeric)

colnames(Q4) <- c("Id", q1_chain4$Q1)

unique(q1_chain4$Q4)

Q4_long <- Q4 %>%
  gather(Q1, value, -1) %>%
  drop_na()

Q4_final <- merge(Q4_long, q1_chain4, by = c("Q1"), all.x = TRUE)

Q4_final <- Q4_final %>%
  select(-1) %>%
  mutate_if(is.character, str_trim)

Q4_final$value <- gsub("1", "Poor_quality", Q4_final$value)
Q4_final$value <- gsub("2", "Somewhat_low_quality", Q4_final$value)
Q4_final$value <- gsub("3", "Standard_quality", Q4_final$value)
Q4_final$value <- gsub("4", "Good_quality", Q4_final$value)
Q4_final$value <- gsub("5", "High_quality", Q4_final$value)

Q4_final$value <- as.factor(Q4_final$value)

glimpse(Q4_final)


Q4_final$offer2 <- Q4_final$offer

Q4_final$offer2 <- str_replace_all(Q4_final$offer2, "[[:punct:]]", " ")
Q4_final$offer2 <- tolower(Q4_final$offer2)


Q4_final$brand2 <- Q4_final$brand


Q4_final$brand2 <- str_replace_all(Q4_final$brand2, "[[:punct:]]", " ")
Q4_final$brand2 <- tolower(Q4_final$brand2)


glimpse(Q1_final2)


Q4_final <- Q4_final %>%
  mutate_if(is.character, str_trim)


Q4_final2 <- merge(Q4_final, description_map, by = c("offer2"), all.x = TRUE)

Q4_final2 <- Q4_final2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q4_final2)[which(names(Q4_final2) == "offer.x")] <- "offer"

colnames(Q4_final2)[which(names(Q4_final2) == "Company")] <- "chain"

Q4_final2$`Month Tested` <- ifelse(Q4_final2$`Month Tested` == "Client", "Client", "ALL")

########################################### Q5 june below

Q5_june <- lto_june %>%
  select(Respondent_Serial, 1026:1147)


colnames(Q5_june) <- c("Id", q1_chain_june4$Q1)

Q5_long_june <- Q5_june %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q5_long_june)

## now get the offer, description
## left join Q1_long with q1_chain4



Q5_final_june <- merge(Q5_long_june, q1_chain_june4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_june)


Q5_final_june <- Q5_final_june %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q5_final_june$value <- gsub("1", "much_less_likely", Q5_final_june$value)
Q5_final_june$value <- gsub("2", "less_likely", Q5_final_june$value)
Q5_final_june$value <- gsub("3", "neither_more_nor_less", Q5_final_june$value)
Q5_final_june$value <- gsub("4", "more_likely", Q5_final_june$value)
Q5_final_june$value <- gsub("5", "much_more_likely", Q5_final_june$value)

Q5_final_june$value <- as.factor(Q5_final_june$value)

Q5_final_june$offer2 <- Q5_final_june$offer

Q5_final_june$offer2 <- str_replace_all(Q5_final_june$offer2, "[[:punct:]]", " ")
Q5_final_june$offer2 <- tolower(Q5_final_june$offer2)


Q5_final_june$brand2 <- Q5_final_june$brand


Q5_final_june$brand2 <- str_replace_all(Q5_final_june$brand2, "[[:punct:]]", " ")
Q5_final_june$brand2 <- tolower(Q5_final_june$brand2)

Q5_final_june <- Q5_final_june %>%
  mutate_if(is.character, str_trim)


Q5_final_june2 <- merge(Q5_final_june, description_map_june, by = c("offer2"), all.x = TRUE)

glimpse(Q5_final_june2)

Q5_final_june2 <- Q5_final_june2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q5_final_june2)[which(names(Q5_final_june2) == "offer.x")] <- "offer"

colnames(Q5_final_june2)[which(names(Q5_final_june2) == "Company")] <- "chain"

Q5_final_june2$`Month Tested` <- ifelse(Q5_final_june2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q5 june above 




########################################### Q5 july below

Q5_july <- lto_july %>%
  select(Respondent_Serial, 1022:1122)


colnames(Q5_july) <- c("Id", q1_chain_july4$Q1)

Q5_long_july <- Q5_july %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q5_long_july)

## now get the offer, description
## left join Q1_long with q1_chain4



Q5_final_july <- merge(Q5_long_july, q1_chain_july4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_july)


Q5_final_july <- Q5_final_july %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q5_final_july$value <- gsub("1", "much_less_likely", Q5_final_july$value)
Q5_final_july$value <- gsub("2", "less_likely", Q5_final_july$value)
Q5_final_july$value <- gsub("3", "neither_more_nor_less", Q5_final_july$value)
Q5_final_july$value <- gsub("4", "more_likely", Q5_final_july$value)
Q5_final_july$value <- gsub("5", "much_more_likely", Q5_final_july$value)

Q5_final_july$value <- as.factor(Q5_final_july$value)

Q5_final_july$offer2 <- Q5_final_july$offer

Q5_final_july$offer2 <- str_replace_all(Q5_final_july$offer2, "[[:punct:]]", " ")
Q5_final_july$offer2 <- tolower(Q5_final_july$offer2)


Q5_final_july$brand2 <- Q5_final_july$brand


Q5_final_july$brand2 <- str_replace_all(Q5_final_july$brand2, "[[:punct:]]", " ")
Q5_final_july$brand2 <- tolower(Q5_final_july$brand2)

Q5_final_july <- Q5_final_july %>%
  mutate_if(is.character, str_trim)


Q5_final_july2 <- merge(Q5_final_july, description_map_july, by = c("offer2"), all.x = TRUE)

glimpse(Q5_final_july2)

Q5_final_july2 <- Q5_final_july2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q5_final_july2)[which(names(Q5_final_july2) == "offer.x")] <- "offer"

colnames(Q5_final_july2)[which(names(Q5_final_july2) == "Company")] <- "chain"

Q5_final_july2$`Month Tested` <- as.character(Q5_final_july2$`Month Tested`)


Q5_final_july2$`Month Tested` <- ifelse(Q5_final_july2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q5 july above 



########################################### Q5 aug below

Q5_aug <- lto_aug %>%
  select(Respondent_Serial, 884:979)


colnames(Q5_aug) <- c("Id", q1_chain_aug4$Q1)

Q5_long_aug <- Q5_aug %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q5_long_aug)

## now get the offer, description
## left join Q1_long with q1_chain4



Q5_final_aug <- merge(Q5_long_aug, q1_chain_aug4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_aug)


Q5_final_aug <- Q5_final_aug %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q5_final_aug$value <- gsub("1", "much_less_likely", Q5_final_aug$value)
Q5_final_aug$value <- gsub("2", "less_likely", Q5_final_aug$value)
Q5_final_aug$value <- gsub("3", "neither_more_nor_less", Q5_final_aug$value)
Q5_final_aug$value <- gsub("4", "more_likely", Q5_final_aug$value)
Q5_final_aug$value <- gsub("5", "much_more_likely", Q5_final_aug$value)

Q5_final_aug$value <- as.factor(Q5_final_aug$value)

Q5_final_aug$offer2 <- Q5_final_aug$offer

Q5_final_aug$offer2 <- str_replace_all(Q5_final_aug$offer2, "[[:punct:]]", " ")
Q5_final_aug$offer2 <- tolower(Q5_final_aug$offer2)


Q5_final_aug$brand2 <- Q5_final_aug$brand


Q5_final_aug$brand2 <- str_replace_all(Q5_final_aug$brand2, "[[:punct:]]", " ")
Q5_final_aug$brand2 <- tolower(Q5_final_aug$brand2)

Q5_final_aug <- Q5_final_aug %>%
  mutate_if(is.character, str_trim)


## make changes here
Q5_final_aug$offer2 <- ifelse(Q5_final_aug$brand == "Checkers Drive-In Restaurants", "1$ Famous Seasoned Fries", Q5_final_aug$offer2)


## remove space between the sentence
Q5_final_aug$offer2 <- gsub('\\s+', '', Q5_final_aug$offer2)


Q5_final_aug2 <- merge(Q5_final_aug, description_map_aug, by = c("offer2"), all.x = TRUE)

glimpse(Q5_final_aug2)

Q5_final_aug2 <- Q5_final_aug2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q5_final_aug2)[which(names(Q5_final_aug2) == "offer.x")] <- "offer"

colnames(Q5_final_aug2)[which(names(Q5_final_aug2) == "Company")] <- "chain"

Q5_final_aug2$`Month Tested` <- as.character(Q5_final_aug2$`Month Tested`)


Q5_final_aug2$`Month Tested` <- ifelse(Q5_final_aug2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q5 aug above 









#########################################################################################################


Q5 <- lto %>%
  select(Respondent_Serial, 1110:1252) #%>%
#select_if(is.numeric)

colnames(Q5) <- c("Id", q1_chain4$Q1)

unique(q1_chain4$Q5)

Q5_long <- Q5 %>%
  gather(Q1, value, -1) %>%
  drop_na()

Q5_final <- merge(Q5_long, q1_chain4, by = c("Q1"), all.x = TRUE)

Q5_final <- Q5_final %>%
  select(-1) %>%
  mutate_if(is.character, str_trim)

Q5_final$value <- gsub("1", "much_less_likely", Q5_final$value)
Q5_final$value <- gsub("2", "less_likely", Q5_final$value)
Q5_final$value <- gsub("3", "neither_more_nor_less", Q5_final$value)
Q5_final$value <- gsub("4", "more_likely", Q5_final$value)
Q5_final$value <- gsub("5", "much_more_likely", Q5_final$value)

Q5_final$value <- as.factor(Q5_final$value)

glimpse(Q5_final)


Q5_final$offer2 <- Q5_final$offer

Q5_final$offer2 <- str_replace_all(Q5_final$offer2, "[[:punct:]]", " ")
Q5_final$offer2 <- tolower(Q5_final$offer2)


Q5_final$brand2 <- Q5_final$brand


Q5_final$brand2 <- str_replace_all(Q5_final$brand2, "[[:punct:]]", " ")
Q5_final$brand2 <- tolower(Q5_final$brand2)


glimpse(Q1_final2)

Q5_final <- Q5_final %>%
  mutate_if(is.character, str_trim)

Q5_final2 <- merge(Q5_final, description_map, by = c("offer2"), all.x = TRUE)

Q5_final2 <- Q5_final2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q5_final2)[which(names(Q5_final2) == "offer.x")] <- "offer"

colnames(Q5_final2)[which(names(Q5_final2) == "Company")] <- "chain"

Q5_final2$`Month Tested` <- ifelse(Q5_final2$`Month Tested` == "Client", "Client", "ALL")


########################################### Q6 june below

Q6_june <- lto_june %>%
  select(Respondent_Serial, 1148:1269)


colnames(Q6_june) <- c("Id", q1_chain_june4$Q1)

Q6_long_june <- Q6_june %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q6_long_june)

## now get the offer, description
## left join Q1_long with q1_chain4

Q6_final_june <- merge(Q6_long_june, q1_chain_june4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_june)


Q6_final_june <- Q6_final_june %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q6_final_june$value <- gsub("1", "unsure", Q6_final_june$value)
Q6_final_june$value <- gsub("2", "only_once", Q6_final_june$value)
Q6_final_june$value <- gsub("3", "some_visits", Q6_final_june$value)
Q6_final_june$value <- gsub("4", "mostly", Q6_final_june$value)
Q6_final_june$value <- gsub("5", "everytime", Q6_final_june$value)

Q6_final_june$value <- as.factor(Q6_final_june$value)

Q6_final_june$offer2 <- Q6_final_june$offer

Q6_final_june$offer2 <- str_replace_all(Q6_final_june$offer2, "[[:punct:]]", " ")
Q6_final_june$offer2 <- tolower(Q6_final_june$offer2)


Q6_final_june$brand2 <- Q6_final_june$brand


Q6_final_june$brand2 <- str_replace_all(Q6_final_june$brand2, "[[:punct:]]", " ")
Q6_final_june$brand2 <- tolower(Q6_final_june$brand2)

Q6_final_june <- Q6_final_june %>%
  mutate_if(is.character, str_trim)


Q6_final_june2 <- merge(Q6_final_june, description_map_june, by = c("offer2"), all.x = TRUE)

glimpse(Q6_final_june2)

Q6_final_june2 <- Q6_final_june2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q6_final_june2)[which(names(Q6_final_june2) == "offer.x")] <- "offer"

colnames(Q6_final_june2)[which(names(Q6_final_june2) == "Company")] <- "chain"

Q6_final_june2$`Month Tested` <- ifelse(Q6_final_june2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q6 june above 





########################################### Q6 july below

Q6_july <- lto_july %>%
  select(Respondent_Serial, 1123:1223)


colnames(Q6_july) <- c("Id", q1_chain_july4$Q1)

Q6_long_july <- Q6_july %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q6_long_july)

## now get the offer, description
## left join Q1_long with q1_chain4

Q6_final_july <- merge(Q6_long_july, q1_chain_july4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_july)


Q6_final_july <- Q6_final_july %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q6_final_july$value <- gsub("1", "unsure", Q6_final_july$value)
Q6_final_july$value <- gsub("2", "only_once", Q6_final_july$value)
Q6_final_july$value <- gsub("3", "some_visits", Q6_final_july$value)
Q6_final_july$value <- gsub("4", "mostly", Q6_final_july$value)
Q6_final_july$value <- gsub("5", "everytime", Q6_final_july$value)

Q6_final_july$value <- as.factor(Q6_final_july$value)

Q6_final_july$offer2 <- Q6_final_july$offer

Q6_final_july$offer2 <- str_replace_all(Q6_final_july$offer2, "[[:punct:]]", " ")
Q6_final_july$offer2 <- tolower(Q6_final_july$offer2)


Q6_final_july$brand2 <- Q6_final_july$brand


Q6_final_july$brand2 <- str_replace_all(Q6_final_july$brand2, "[[:punct:]]", " ")
Q6_final_july$brand2 <- tolower(Q6_final_july$brand2)

Q6_final_july <- Q6_final_july %>%
  mutate_if(is.character, str_trim)


Q6_final_july2 <- merge(Q6_final_july, description_map_july, by = c("offer2"), all.x = TRUE)

glimpse(Q6_final_july2)

Q6_final_july2 <- Q6_final_july2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q6_final_july2)[which(names(Q6_final_july2) == "offer.x")] <- "offer"

colnames(Q6_final_july2)[which(names(Q6_final_july2) == "Company")] <- "chain"

Q6_final_july2$`Month Tested` <- as.character(Q6_final_july2$`Month Tested`)

Q6_final_july2$`Month Tested` <- ifelse(Q6_final_july2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q6 july above 

########################################### Q6 aug below

Q6_aug <- lto_aug %>%
  select(Respondent_Serial, 980:1075)


colnames(Q6_aug) <- c("Id", q1_chain_aug4$Q1)

Q6_long_aug <- Q6_aug %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q6_long_aug)

## now get the offer, description
## left join Q1_long with q1_chain4

Q6_final_aug <- merge(Q6_long_aug, q1_chain_aug4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_aug)


Q6_final_aug <- Q6_final_aug %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q6_final_aug$value <- gsub("1", "unsure", Q6_final_aug$value)
Q6_final_aug$value <- gsub("2", "only_once", Q6_final_aug$value)
Q6_final_aug$value <- gsub("3", "some_visits", Q6_final_aug$value)
Q6_final_aug$value <- gsub("4", "mostly", Q6_final_aug$value)
Q6_final_aug$value <- gsub("5", "everytime", Q6_final_aug$value)

Q6_final_aug$value <- as.factor(Q6_final_aug$value)

Q6_final_aug$offer2 <- Q6_final_aug$offer

Q6_final_aug$offer2 <- str_replace_all(Q6_final_aug$offer2, "[[:punct:]]", " ")
Q6_final_aug$offer2 <- tolower(Q6_final_aug$offer2)


Q6_final_aug$brand2 <- Q6_final_aug$brand


Q6_final_aug$brand2 <- str_replace_all(Q6_final_aug$brand2, "[[:punct:]]", " ")
Q6_final_aug$brand2 <- tolower(Q6_final_aug$brand2)


## make changes here
Q6_final_aug$offer2 <- ifelse(Q6_final_aug$brand == "Checkers Drive-In Restaurants", "1$ Famous Seasoned Fries", Q6_final_aug$offer2)


## remove space between the sentence
Q6_final_aug$offer2 <- gsub('\\s+', '', Q6_final_aug$offer2)


Q6_final_aug <- Q6_final_aug %>%
  mutate_if(is.character, str_trim)


Q6_final_aug2 <- merge(Q6_final_aug, description_map_aug, by = c("offer2"), all.x = TRUE)

glimpse(Q6_final_aug2)

Q6_final_aug2 <- Q6_final_aug2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q6_final_aug2)[which(names(Q6_final_aug2) == "offer.x")] <- "offer"

colnames(Q6_final_aug2)[which(names(Q6_final_aug2) == "Company")] <- "chain"

Q6_final_aug2$`Month Tested` <- as.character(Q6_final_aug2$`Month Tested`)

Q6_final_aug2$`Month Tested` <- ifelse(Q6_final_aug2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q6 aug above 

Q6 <- lto %>%
  select(Respondent_Serial, 1253:1395) #%>%
#select_if(is.numeric)

colnames(Q6) <- c("Id", q1_chain4$Q1)

unique(q1_chain4$Q6)

Q6_long <- Q6 %>%
  gather(Q1, value, -1) %>%
  drop_na()

Q6_final <- merge(Q6_long, q1_chain4, by = c("Q1"), all.x = TRUE)

Q6_final <- Q6_final %>%
  select(-1) %>%
  mutate_if(is.character, str_trim)

Q6_final$value <- gsub("1", "unsure", Q6_final$value)
Q6_final$value <- gsub("2", "only_once", Q6_final$value)
Q6_final$value <- gsub("3", "some_visits", Q6_final$value)
Q6_final$value <- gsub("4", "mostly", Q6_final$value)
Q6_final$value <- gsub("5", "everytime", Q6_final$value)

Q6_final$value <- as.factor(Q6_final$value)

glimpse(Q6_final)


Q6_final$offer2 <- Q6_final$offer

Q6_final$offer2 <- str_replace_all(Q6_final$offer2, "[[:punct:]]", " ")
Q6_final$offer2 <- tolower(Q6_final$offer2)


Q6_final$brand2 <- Q6_final$brand


Q6_final$brand2 <- str_replace_all(Q6_final$brand2, "[[:punct:]]", " ")
Q6_final$brand2 <- tolower(Q6_final$brand2)


glimpse(Q1_final2)

Q6_final <- Q6_final %>%
  mutate_if(is.character, str_trim)

Q6_final2 <- merge(Q6_final, description_map, by = c("offer2"), all.x = TRUE)

Q6_final2 <- Q6_final2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q6_final2)[which(names(Q6_final2) == "offer.x")] <- "offer"

colnames(Q6_final2)[which(names(Q6_final2) == "Company")] <- "chain"

Q6_final2$`Month Tested` <- ifelse(Q6_final2$`Month Tested` == "Client", "Client", "ALL")

########################################### Q7 june below

Q7_june <- lto_june %>%
  select(Respondent_Serial, 1270:1391)


colnames(Q7_june) <- c("Id", q1_chain_june4$Q1)

Q7_long_june <- Q7_june %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q7_long_june)

## now get the offer, description
## left join Q1_long with q1_chain4

Q7_final_june <- merge(Q7_long_june, q1_chain_june4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_june)


Q7_final_june <- Q7_final_june %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q7_final_june$value <- gsub("1", "wouldnt_crave_offer", Q7_final_june$value)
Q7_final_june$value <- gsub("2", "2", Q7_final_june$value)
Q7_final_june$value <- gsub("3", "3", Q7_final_june$value)
Q7_final_june$value <- gsub("4", "4", Q7_final_june$value)
Q7_final_june$value <- gsub("5", "extermely_craving", Q7_final_june$value)

Q7_final_june$value <- as.factor(Q7_final_june$value)

Q7_final_june$offer2 <- Q7_final_june$offer

Q7_final_june$offer2 <- str_replace_all(Q7_final_june$offer2, "[[:punct:]]", " ")
Q7_final_june$offer2 <- tolower(Q7_final_june$offer2)


Q7_final_june$brand2 <- Q7_final_june$brand


Q7_final_june$brand2 <- str_replace_all(Q7_final_june$brand2, "[[:punct:]]", " ")
Q7_final_june$brand2 <- tolower(Q7_final_june$brand2)


Q7_final_june <- Q7_final_june %>%
  mutate_if(is.character, str_trim)


Q7_final_june2 <- merge(Q7_final_june, description_map_june, by = c("offer2"), all.x = TRUE)

glimpse(Q7_final_june2)

Q7_final_june2 <- Q7_final_june2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q7_final_june2)[which(names(Q7_final_june2) == "offer.x")] <- "offer"

colnames(Q7_final_june2)[which(names(Q7_final_june2) == "Company")] <- "chain"

Q7_final_june2$`Month Tested` <- ifelse(Q7_final_june2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q7 june above 

########################################### Q7 july below

Q7_july <- lto_july %>%
  select(Respondent_Serial, 1224:1324)


colnames(Q7_july) <- c("Id", q1_chain_july4$Q1)

Q7_long_july <- Q7_july %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q7_long_july)

## now get the offer, description
## left join Q1_long with q1_chain4

Q7_final_july <- merge(Q7_long_july, q1_chain_july4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_july)


Q7_final_july <- Q7_final_july %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q7_final_july$value <- gsub("1", "wouldnt_crave_offer", Q7_final_july$value)
Q7_final_july$value <- gsub("2", "2", Q7_final_july$value)
Q7_final_july$value <- gsub("3", "3", Q7_final_july$value)
Q7_final_july$value <- gsub("4", "4", Q7_final_july$value)
Q7_final_july$value <- gsub("5", "extermely_craving", Q7_final_july$value)

Q7_final_july$value <- as.factor(Q7_final_july$value)

Q7_final_july$offer2 <- Q7_final_july$offer

Q7_final_july$offer2 <- str_replace_all(Q7_final_july$offer2, "[[:punct:]]", " ")
Q7_final_july$offer2 <- tolower(Q7_final_july$offer2)


Q7_final_july$brand2 <- Q7_final_july$brand


Q7_final_july$brand2 <- str_replace_all(Q7_final_july$brand2, "[[:punct:]]", " ")
Q7_final_july$brand2 <- tolower(Q7_final_july$brand2)

Q7_final_july <- Q7_final_july %>%
  mutate_if(is.character, str_trim)


Q7_final_july2 <- merge(Q7_final_july, description_map_july, by = c("offer2"), all.x = TRUE)

glimpse(Q7_final_july2)

Q7_final_july2 <- Q7_final_july2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q7_final_july2)[which(names(Q7_final_july2) == "offer.x")] <- "offer"

colnames(Q7_final_july2)[which(names(Q7_final_july2) == "Company")] <- "chain"


Q7_final_july2$`Month Tested` <- as.character(Q7_final_july2$`Month Tested`)

Q7_final_july2$`Month Tested` <- ifelse(Q7_final_july2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q7 july above

########################################### Q7 aug below

Q7_aug <- lto_aug %>%
  select(Respondent_Serial, 1076:1171)


colnames(Q7_aug) <- c("Id", q1_chain_aug4$Q1)

Q7_long_aug <- Q7_aug %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q7_long_aug)

## now get the offer, description
## left join Q1_long with q1_chain4

Q7_final_aug <- merge(Q7_long_aug, q1_chain_aug4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_aug)


Q7_final_aug <- Q7_final_aug %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)



Q7_final_aug$value <- gsub("1", "wouldnt_crave_offer", Q7_final_aug$value)
Q7_final_aug$value <- gsub("2", "2", Q7_final_aug$value)
Q7_final_aug$value <- gsub("3", "3", Q7_final_aug$value)
Q7_final_aug$value <- gsub("4", "4", Q7_final_aug$value)
Q7_final_aug$value <- gsub("5", "extermely_craving", Q7_final_aug$value)

Q7_final_aug$value <- as.factor(Q7_final_aug$value)

Q7_final_aug$offer2 <- Q7_final_aug$offer

Q7_final_aug$offer2 <- str_replace_all(Q7_final_aug$offer2, "[[:punct:]]", " ")
Q7_final_aug$offer2 <- tolower(Q7_final_aug$offer2)


Q7_final_aug$brand2 <- Q7_final_aug$brand


Q7_final_aug$brand2 <- str_replace_all(Q7_final_aug$brand2, "[[:punct:]]", " ")
Q7_final_aug$brand2 <- tolower(Q7_final_aug$brand2)

## make changes here
Q7_final_aug$offer2 <- ifelse(Q7_final_aug$brand == "Checkers Drive-In Restaurants", "1$ Famous Seasoned Fries", Q7_final_aug$offer2)


## remove space between the sentence
Q7_final_aug$offer2 <- gsub('\\s+', '', Q7_final_aug$offer2)



Q7_final_aug <- Q7_final_aug %>%
  mutate_if(is.character, str_trim)


Q7_final_aug2 <- merge(Q7_final_aug, description_map_aug, by = c("offer2"), all.x = TRUE)

glimpse(Q7_final_aug2)

Q7_final_aug2 <- Q7_final_aug2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q7_final_aug2)[which(names(Q7_final_aug2) == "offer.x")] <- "offer"

colnames(Q7_final_aug2)[which(names(Q7_final_aug2) == "Company")] <- "chain"


Q7_final_aug2$`Month Tested` <- as.character(Q7_final_aug2$`Month Tested`)

Q7_final_aug2$`Month Tested` <- ifelse(Q7_final_aug2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q7 aug above

Q7 <- lto %>%
  select(Respondent_Serial, 1396:1538) #%>%
#select_if(is.numeric)

colnames(Q7) <- c("Id", q1_chain4$Q1)

unique(q1_chain4$Q7)

Q7_long <- Q7 %>%
  gather(Q1, value, -1) %>%
  drop_na()

Q7_final <- merge(Q7_long, q1_chain4, by = c("Q1"), all.x = TRUE)

Q7_final <- Q7_final %>%
  select(-1) %>%
  mutate_if(is.character, str_trim)

Q7_final$value <- gsub("1", "wouldnt_crave_offer", Q7_final$value)
Q7_final$value <- gsub("2", "2", Q7_final$value)
Q7_final$value <- gsub("3", "3", Q7_final$value)
Q7_final$value <- gsub("4", "4", Q7_final$value)
Q7_final$value <- gsub("5", "extermely_craving", Q7_final$value)

Q7_final$value <- as.factor(Q7_final$value)

glimpse(Q7_final)


Q7_final$offer2 <- Q7_final$offer

Q7_final$offer2 <- str_replace_all(Q7_final$offer2, "[[:punct:]]", " ")
Q7_final$offer2 <- tolower(Q7_final$offer2)


Q7_final$brand2 <- Q7_final$brand


Q7_final$brand2 <- str_replace_all(Q7_final$brand2, "[[:punct:]]", " ")
Q7_final$brand2 <- tolower(Q7_final$brand2)


glimpse(Q1_final2)


## make changes here
Q7_final_aug$offer2 <- ifelse(Q7_final_aug$brand == "Checkers Drive-In Restaurants", "1$ Famous Seasoned Fries", Q7_final_aug$offer2)


## remove space between the sentence
Q7_final_aug$offer2 <- gsub('\\s+', '', Q7_final_aug$offer2)


Q7_final <- Q7_final %>%
  mutate_if(is.character, str_trim)


Q7_final2 <- merge(Q7_final, description_map, by = c("offer2"), all.x = TRUE)

Q7_final2 <- Q7_final2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q7_final2)[which(names(Q7_final2) == "offer.x")] <- "offer"

colnames(Q7_final2)[which(names(Q7_final2) == "Company")] <- "chain"

Q7_final2$`Month Tested` <- ifelse(Q7_final2$`Month Tested` == "Client", "Client", "ALL")

########################################### Q8 june below

Q8_june <- lto_june %>%
  select(Respondent_Serial, 1392:1513)


colnames(Q8_june) <- c("Id", q1_chain_june4$Q1)

Q8_long_june <- Q8_june %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q8_long_june)

## now get the offer, description
## left join Q1_long with q1_chain4

Q8_final_june <- merge(Q8_long_june, q1_chain_june4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_june)


Q8_final_june <- Q8_final_june %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)


Q8_final_june$value <- gsub("1", "only_offer", Q8_final_june$value)
Q8_final_june$value <- gsub("2", "additional_foods", Q8_final_june$value)
Q8_final_june$value <- gsub("3", "unsure", Q8_final_june$value)

Q8_final_june$value <- as.factor(Q8_final_june$value)

Q8_final_june$offer2 <- Q8_final_june$offer

Q8_final_june$offer2 <- str_replace_all(Q8_final_june$offer2, "[[:punct:]]", " ")
Q8_final_june$offer2 <- tolower(Q8_final_june$offer2)


Q8_final_june$brand2 <- Q8_final_june$brand


Q8_final_june$brand2 <- str_replace_all(Q8_final_june$brand2, "[[:punct:]]", " ")
Q8_final_june$brand2 <- tolower(Q8_final_june$brand2)

Q8_final_june <- Q8_final_june %>%
  mutate_if(is.character, str_trim)


Q8_final_june2 <- merge(Q8_final_june, description_map_june, by = c("offer2"), all.x = TRUE)

glimpse(Q8_final_june2)

Q8_final_june2 <- Q8_final_june2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q8_final_june2)[which(names(Q8_final_june2) == "offer.x")] <- "offer"

colnames(Q8_final_june2)[which(names(Q8_final_june2) == "Company")] <- "chain"

Q8_final_june2$`Month Tested` <- ifelse(Q8_final_june2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q8 june above 

########################################### Q8 july below

Q8_july <- lto_july %>%
  select(Respondent_Serial, 1325:1425)


colnames(Q8_july) <- c("Id", q1_chain_july4$Q1)

Q8_long_july <- Q8_july %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q8_long_july)

## now get the offer, description
## left join Q1_long with q1_chain4

Q8_final_july <- merge(Q8_long_july, q1_chain_july4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_july)


Q8_final_july <- Q8_final_july %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)


Q8_final_july$value <- gsub("1", "only_offer", Q8_final_july$value)
Q8_final_july$value <- gsub("2", "additional_foods", Q8_final_july$value)
Q8_final_july$value <- gsub("3", "unsure", Q8_final_july$value)

Q8_final_july$value <- as.factor(Q8_final_july$value)

Q8_final_july$offer2 <- Q8_final_july$offer

Q8_final_july$offer2 <- str_replace_all(Q8_final_july$offer2, "[[:punct:]]", " ")
Q8_final_july$offer2 <- tolower(Q8_final_july$offer2)


Q8_final_july$brand2 <- Q8_final_july$brand


Q8_final_july$brand2 <- str_replace_all(Q8_final_july$brand2, "[[:punct:]]", " ")
Q8_final_july$brand2 <- tolower(Q8_final_july$brand2)

Q8_final_july <- Q8_final_july %>%
  mutate_if(is.character, str_trim)


Q8_final_july2 <- merge(Q8_final_july, description_map_july, by = c("offer2"), all.x = TRUE)

glimpse(Q8_final_july2)

Q8_final_july2 <- Q8_final_july2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q8_final_july2)[which(names(Q8_final_july2) == "offer.x")] <- "offer"

colnames(Q8_final_july2)[which(names(Q8_final_july2) == "Company")] <- "chain"

Q8_final_july2$`Month Tested` <- as.character(Q8_final_july2$`Month Tested`)

Q8_final_july2$`Month Tested` <- ifelse(Q8_final_july2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q8 july above 


########################################### Q8 aug below

Q8_aug <- lto_aug %>%
  select(Respondent_Serial, 1172:1267)


colnames(Q8_aug) <- c("Id", q1_chain_aug4$Q1)

Q8_long_aug <- Q8_aug %>% 
  gather(Q1, value, -1) %>%
  drop_na()


glimpse(Q8_long_aug)

## now get the offer, description
## left join Q1_long with q1_chain4

Q8_final_aug <- merge(Q8_long_aug, q1_chain_aug4, by = c("Q1"), all.x = TRUE)

glimpse(Q1_final_aug)


Q8_final_aug <- Q8_final_aug %>%
  #select(-1) %>%
  mutate_if(is.character, str_trim)


Q8_final_aug$value <- gsub("1", "only_offer", Q8_final_aug$value)
Q8_final_aug$value <- gsub("2", "additional_foods", Q8_final_aug$value)
Q8_final_aug$value <- gsub("3", "unsure", Q8_final_aug$value)

Q8_final_aug$value <- as.factor(Q8_final_aug$value)

Q8_final_aug$offer2 <- Q8_final_aug$offer

Q8_final_aug$offer2 <- str_replace_all(Q8_final_aug$offer2, "[[:punct:]]", " ")
Q8_final_aug$offer2 <- tolower(Q8_final_aug$offer2)


Q8_final_aug$brand2 <- Q8_final_aug$brand


Q8_final_aug$brand2 <- str_replace_all(Q8_final_aug$brand2, "[[:punct:]]", " ")
Q8_final_aug$brand2 <- tolower(Q8_final_aug$brand2)

## make changes here
Q8_final_aug$offer2 <- ifelse(Q8_final_aug$brand == "Checkers Drive-In Restaurants", "1$ Famous Seasoned Fries", Q8_final_aug$offer2)


## remove space between the sentence
Q8_final_aug$offer2 <- gsub('\\s+', '', Q8_final_aug$offer2)



Q8_final_aug <- Q8_final_aug %>%
  mutate_if(is.character, str_trim)


Q8_final_aug2 <- merge(Q8_final_aug, description_map_aug, by = c("offer2"), all.x = TRUE)

glimpse(Q8_final_aug2)

Q8_final_aug2 <- Q8_final_aug2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q8_final_aug2)[which(names(Q8_final_aug2) == "offer.x")] <- "offer"

colnames(Q8_final_aug2)[which(names(Q8_final_aug2) == "Company")] <- "chain"

Q8_final_aug2$`Month Tested` <- as.character(Q8_final_aug2$`Month Tested`)

Q8_final_aug2$`Month Tested` <- ifelse(Q8_final_aug2$`Month Tested` == "Client", "Client", "ALL")

################################################# Q8 aug above 




Q8 <- lto %>%
  select(Respondent_Serial, 1539:1681) #%>%
#select_if(is.numeric)

colnames(Q8) <- c("Id", q1_chain4$Q1)

unique(q1_chain4$Q8)

Q8_long <- Q8 %>%
  gather(Q1, value, -1) %>%
  drop_na()

Q8_final <- merge(Q8_long, q1_chain4, by = c("Q1"), all.x = TRUE)

Q8_final <- Q8_final %>%
  select(-1) %>%
  mutate_if(is.character, str_trim)

Q8_final$value <- gsub("1", "only_offer", Q8_final$value)
Q8_final$value <- gsub("2", "additional_foods", Q8_final$value)
Q8_final$value <- gsub("3", "unsure", Q8_final$value)

Q8_final$value <- as.factor(Q8_final$value)

glimpse(Q8_final)


Q8_final$offer2 <- Q8_final$offer

Q8_final$offer2 <- str_replace_all(Q8_final$offer2, "[[:punct:]]", " ")
Q8_final$offer2 <- tolower(Q8_final$offer2)


Q8_final$brand2 <- Q8_final$brand


Q8_final$brand2 <- str_replace_all(Q8_final$brand2, "[[:punct:]]", " ")
Q8_final$brand2 <- tolower(Q8_final$brand2)


glimpse(Q1_final2)


Q8_final <- Q8_final %>%
  mutate_if(is.character, str_trim)

Q8_final2 <- merge(Q8_final, description_map, by = c("offer2"), all.x = TRUE)

Q8_final2 <- Q8_final2 %>%
  select(Id, Company, offer.x, Description,Segment, value, `Month Tested`) %>%
  mutate_if(is.character, as.factor)


colnames(Q8_final2)[which(names(Q8_final2) == "offer.x")] <- "offer"

colnames(Q8_final2)[which(names(Q8_final2) == "Company")] <- "chain"

Q8_final2$`Month Tested` <- ifelse(Q8_final2$`Month Tested` == "Client", "Client", "ALL")



######################### rbind combine all the questions (May, June, july, aug) ##############

Q1_final2 <- rbind(Q1_final2, Q1_final_june2, Q1_final_july2, Q1_final_aug2) %>%
  mutate_if(is.factor, as.character) %>%
  mutate(Mon = ifelse(Id %in% may_Id, "May", ifelse(Id %in% july_Id, "July", ifelse(Id %in% aug_Id, "August", "June"))))

Q1_final2$Segment <- as.factor(Q1_final2$Segment)
Q1_final2$`Month Tested` <- as.factor(Q1_final2$`Month Tested`)


Q2_final2 <- rbind(Q2_final2, Q2_final_june2, Q2_final_july2, Q2_final_aug2) %>%
  mutate_if(is.factor, as.character) %>%
  mutate(Mon = ifelse(Id %in% may_Id, "May", ifelse(Id %in% july_Id, "July", ifelse(Id %in% aug_Id, "August", "June"))))

Q2_final2$Segment <- as.factor(Q2_final2$Segment)
Q2_final2$`Month Tested` <- as.factor(Q2_final2$`Month Tested`)


Q3_final2 <- rbind(Q3_final2, Q3_final_june2, Q3_final_july2, Q3_final_aug2) %>%
  mutate_if(is.factor, as.character) %>%
  mutate(Mon = ifelse(Id %in% may_Id, "May", ifelse(Id %in% july_Id, "July", ifelse(Id %in% aug_Id, "August", "June"))))

Q3_final2$Segment <- as.factor(Q3_final2$Segment)
Q3_final2$`Month Tested` <- as.factor(Q3_final2$`Month Tested`)

Q4_final2 <- rbind(Q4_final2, Q4_final_june2, Q4_final_july2, Q4_final_aug2) %>%
  mutate_if(is.factor, as.character) %>%
  mutate(Mon = ifelse(Id %in% may_Id, "May", ifelse(Id %in% july_Id, "July", ifelse(Id %in% aug_Id, "August", "June"))))

Q4_final2$Segment <- as.factor(Q4_final2$Segment)
Q4_final2$`Month Tested` <- as.factor(Q4_final2$`Month Tested`)

Q5_final2 <- rbind(Q5_final2, Q5_final_june2, Q5_final_july2, Q5_final_aug2) %>%
  mutate_if(is.factor, as.character) %>%
  mutate(Mon = ifelse(Id %in% may_Id, "May", ifelse(Id %in% july_Id, "July", ifelse(Id %in% aug_Id, "August", "June"))))

Q5_final2$Segment <- as.factor(Q5_final2$Segment)
Q5_final2$`Month Tested` <- as.factor(Q5_final2$`Month Tested`)

Q6_final2 <- rbind(Q6_final2, Q6_final_june2, Q6_final_july2, Q6_final_aug2) %>%
  mutate_if(is.factor, as.character) %>%
  mutate(Mon = ifelse(Id %in% may_Id, "May", ifelse(Id %in% july_Id, "July", ifelse(Id %in% aug_Id, "August", "June"))))

Q6_final2$Segment <- as.factor(Q6_final2$Segment)
Q6_final2$`Month Tested` <- as.factor(Q6_final2$`Month Tested`)

Q7_final2 <- rbind(Q7_final2, Q7_final_june2, Q7_final_july2, Q7_final_aug2) %>%
  mutate_if(is.factor, as.character) %>%
  mutate(Mon = ifelse(Id %in% may_Id, "May", ifelse(Id %in% july_Id, "July", ifelse(Id %in% aug_Id, "August", "June"))))

Q7_final2$Segment <- as.factor(Q7_final2$Segment)
Q7_final2$`Month Tested` <- as.factor(Q7_final2$`Month Tested`)

Q8_final2 <- rbind(Q8_final2, Q8_final_june2, Q8_final_july2, Q8_final_aug2) %>%
  mutate_if(is.factor, as.character) %>%
  mutate(Mon = ifelse(Id %in% may_Id, "May", ifelse(Id %in% july_Id, "July", ifelse(Id %in% aug_Id, "August", "June"))))

Q8_final2$Segment <- as.factor(Q8_final2$Segment)
Q8_final2$`Month Tested` <- as.factor(Q8_final2$`Month Tested`)

#save to rda file
save(final_filter, Q1_final2, Q2_final2, Q3_final2, Q4_final2, Q5_final2, Q6_final2, Q7_final2, Q8_final2,
     file = "globe.rda")























