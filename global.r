# INFO --------------------------------------------------------------------
# Project: TI project 
# Author: Vignan Reddy Thumu
# Date: 2/25/20
# Description: valassis dashboard

# LIBRARIES ---------------------------------------------------------------
library(shiny)
library(shinydashboard)
library(shinyWidgets) # for pickerinputs
library(shinyjs) # for disabling buttons
library(tidyverse) # for all things tidy
library(highcharter) # for visualizations
library(scales) # for helping visualizations (axes labels and legends)
library(tools) # for toTitleCase and other handy functions
library(datasets) # for the Iris data for this example project
library(shinyalert) ## use it for the info tab..
library(DT) # for data tables
library(plyr) ## to find percentile 
 

#clear the global enviroment
rm(list = ls())
#str(df, list.len=ncol(df))

## set up working directory
#setwd("C:/Users/vthumu/OneDrive - Winsight/Projects/Pepsi Euro/pepsi-euro-dashboard/data")
#setwd("C:/Users/vthumu/OneDrive - Winsight/Desktop/test files here/InterviewExporter.CBM_WRT_2019.sav.20200503.0249")

# RUN THIS AT THE END OF YOUR DATA CLEANING SCRIPT
# save(df1, df2, df3, file = 'file_name.Rda')
#View(data.frame(names(final_output4)))
# 
# #View(final_output[,c(1,233:260)])
# #View(data.frame(names(final_output3)))
# #View(data.frame(names(df)))
# #data.frame(levels(final_output3$segment))

# LOAD DATA ---------------------------------------------------------------
load('globe.rda')
#load('Unaided_Awareness.Rda')

#write.csv(final_df, "final_df.csv", row.names = F)


# this is the Technomic color palette in order of priority 
color.palette <- c("#00AEEF", # blue
                   "#D22630", # red
                   "#43B02A", # green
                   "#FFD600", # yellow
                   "#9083CD", # purple
                   "#FF8200", # orange
                   "#00ACA0", # green blue
                   "#F599B1", # pink
                   "#BFD730", # olive
                   "#B455A0", # magenta
                   "#6B8BD5", # indigo
                   "#B02A30", # dark red
                   "#58D09C" # sea green
)