# INFO --------------------------------------------------------------------
# Project: TI project 
# Author: Vignan Reddy Thumu
# Date: 2/25/20
# Description: valassis dashboard

# NAV BAR PAGE UI ------------------------------------------------------------------
fluidPage( 
  useShinyjs(),  # Set up shinyjs to hide, show toggle, or toggle element in UI i used this to hide some filters in last tab..
  
  tags$head(
    # this loads the CSS style sheet 
    tags$link(rel = "stylesheet", type = "text/css", href = "style.css")
  ),
  
  ## removes all the errors from my app..
  tags$style(type="text/css",
             ".shiny-output-error { visibility: hidden; }",
             ".shiny-output-error:before { visibility: hidden; }"
  ),
  
  navbarPage(id ="navbar",
             title = div(img(src = "Technomic_white_small.png",
                             # this moves the Tehcnomic logo up slightly and thus is more inline with the navbar tabs
                             style = "margin:-3px")
             ), 
             
             # change this title to be descriptive of the project. Start with "Technomic".
             windowTitle = "Value Dashboard",
             
             ## created a fake tab to remove it later            
             tabPanel("Technomic", 
                      value = "remove_tab",
                      #icon = icon("home"),
                      style='padding-left:50px;',
                      
             ),
             
             fluidRow(
               
               column(2, pickerInput(inputId = "gender_inp",
                                     label = "Gender",
                                     choices = levels(final_filter$Gender),
                                     selected = levels(final_filter$Gender),
                                     multiple=TRUE,
                                     options=list(`actions-box` = TRUE)
               )
               ),
               
               column(2, pickerInput(inputId = "ethnicity_inp",
                                     label = "Ethnicity",
                                     choices = c("Asian", "Black/African-American", "Caucasian","Hispanic"),
                                     #selected = levels(final_filter$Ethnicity),
                                     multiple=TRUE,
                                     options=list(`actions-box` = TRUE)
               )
               ),
               
               
               column(2, pickerInput(inputId = "generation_inp",
                                     label = "Generation",
                                     choices = levels(final_filter$Generation),
                                     selected = levels(final_filter$Generation),
                                     multiple=TRUE,
                                     options=list(`actions-box` = TRUE)
               )
               ),
               
               column(2, pickerInput(inputId = "ageGroup_inp",
                                     label = "Age Group",
                                     choices = levels(final_filter$AgeGroup),
                                     selected = levels(final_filter$AgeGroup),
                                     multiple=TRUE,
                                     options=list(`actions-box` = TRUE)
               )
               ),
               
               column(2, pickerInput(inputId = "income2_inp",
                                     label = "Income",
                                     choices = levels(final_filter$Income2),
                                     selected = levels(final_filter$Income2),
                                     multiple=TRUE,
                                     options=list(`actions-box` = TRUE)
               )
               ),
               
               
               column(2, pickerInput(inputId = "household_input",
                                     label = "Household",
                                     choices = c("Just myself/live alone" = "Just_myself",
                                                 "Children aged 17 or younger" = "younger_17",
                                                 "All other adult-only households" = "other_adults"),
                                     selected = c("Just myself/live alone" = "Just_myself",
                                                  "Children aged 17 or younger" = "younger_17",
                                                  "All other adult-only households" = "other_adults"),
                                     multiple=TRUE,
                                     options=list(`actions-box` = TRUE)
               )
               ),
               
               #unique(final_filter$value_seeker)
               #unique(final_filter$segment)
               
               
               column(2, pickerInput(inputId = "valueSeeker_input",
                                     label = "Value Seeker",
                                     choices = c("Pay close attention to menu prices to find best value" = "deals",
                                          "Usually pick restaurants with lower prices" = "lower_prices",
                                          "Seek out restaurants that offer discounts or special deals" = "Pay_attention"#,
                                          #"Never" = "Never"
                                          ),
                                     # selected = c("Pay close attention to menu prices to find best value" = "deals",
                                     #              "Usually pick restaurants with lower prices" = "lower_prices",
                                     #              "Seek out restaurants that offer discounts or special deals" = "Pay_attention"),
                                     multiple=TRUE,
                                     options=list(`actions-box` = TRUE)
               )
               ),
               
               #colnames(final_filter)
               column(2, pickerInput(inputId = "segment_input",
                                     label = "Segment Usage",
                                     choices = c("Quick service" = "QuickService",
                                                 "Fast casual" = "FastCasual",
                                                 "Midscale" = "Midscale",
                                                 "Casual dining" = "Casualdining",
                                                 "C-store" = "Conveniencestores"#,
                                                 #"Never" = "Never"
                                                 ),
                                     # selected = c("Quick service" = "QuickService",
                                     #              "Fast casual" = "FastCasual",
                                     #              "Midscale" = "Midscale",
                                     #              "Casual dining" = "Casualdining",
                                     #              "C-store" = "Conveniencestores"),
                                     multiple=TRUE,
                                     options=list(`actions-box` = TRUE)
               )
               ), 
               
               ## colnames(final_filter) [-1:-20] All the chain names
               
               #View(data.frame(names(final_filter)))
               
               column(2, pickerInput(inputId = "chain_input",
                                     label = "Chain Usage",
                                     choices = c( colnames(final_filter) [-1:-15]),
                                     #selected = c( colnames(final_filter) [-1:-21]),
                                     multiple=TRUE,
                                     options=list(`actions-box` = TRUE)
               )
               ),
               
               column(2, pickerInput(inputId = "main_seginput",
                                     label = "Segment Items",
                                     choices = levels(Q2_final2$Segment),
                                     selected = levels(Q2_final2$Segment),
                                     multiple=TRUE,
                                     options=list(`actions-box` = TRUE)
               )
               ),
               
               column(1, pickerInput(inputId = "month_input",
                                     label = "Month 2020",
                                     choices = levels(final_filter$month),
                                     selected = levels(final_filter$month),
                                     multiple=TRUE,
                                     options=list(`actions-box` = TRUE)
               )
               ), 
               
               column(1, pickerInput(inputId = "client_input",
                                     label = "Client",
                                     choices = levels(Q2_final2$`Month Tested`),
                                     selected = levels(Q2_final2$`Month Tested`),
                                     multiple=TRUE,
                                     options=list(`actions-box` = TRUE)
               )
               ),
               
               
               
               column(2,  awesomeRadio(
                 inputId = "percent_input",
                 label = "Switch", 
                 choices = c("Percentage","Percentile by segment", "Percentile by total database"),
                 selected = "Percentage",
                 status = "primary"
               ))
               
               
             ),
             
             
             tabPanel("Summary", 
                      # value should be the title of the tab followed by "_tab" this is used if any buttons update the current tab selected or to show/hide tabs
                      value = "sum_tab",
                      # do not need icons for any tabs, this is optional. when an app has a homepage, the home icon is used.
                      #icon = icon("home"),
                      # all tab panels will have this padding, it indents the contents of a tab panel without effecting the top nav bar
                      style='padding-left:50px;',
                      # includeMarkdown("creating a Technomic Shiny App.Rmd")
                      fluidRow(
                        
                        column(12, 
                               column(6, 
                                      h2("") # include an appropriate page title
                               ),
                               column(6, # this is the loading graph for when shiny is working, can change the width of these columns if long title
                                      conditionalPanel(condition="$('html').hasClass('shiny-busy')",
                                                       img(src="Loading_200px.gif", height = "50px"))
                               )
                               
                        )
                      ),
                      
                      DT::dataTableOutput("summary_table")
                      
                      
                      
                      
             ), 
          
             tabPanel("Purchase Intent", 
                      # value should be the title of the tab followed by "_tab" this is used if any buttons update the current tab selected or to show/hide tabs
                      value = "pi_tab",
                      # do not need icons for any tabs, this is optional. when an app has a homepage, the home icon is used.
                      #icon = icon("home"),
                      # all tab panels will have this padding, it indents the contents of a tab panel without effecting the top nav bar
                      style='padding-left:50px;',
                      # includeMarkdown("creating a Technomic Shiny App.Rmd")
                      fluidRow(
                        
                        column(12, 
                               column(6, 
                                      h2("") # include an appropriate page title
                               ),
                               column(6, # this is the loading graph for when shiny is working, can change the width of these columns if long title
                                      conditionalPanel(condition="$('html').hasClass('shiny-busy')",
                                                       img(src="Loading_200px.gif", height = "50px"))
                               )
                               
                        )
                      ),
                      
                      
                      DT::dataTableOutput("purchase_intent_table")
                      
                      
                      
                      
             ),
             
             tabPanel("Value", 
                      # value should be the title of the tab followed by "_tab" this is used if any buttons update the current tab selected or to show/hide tabs
                      value = "val_tab",
                      # do not need icons for any tabs, this is optional. when an app has a homepage, the home icon is used.
                      #icon = icon("home"),
                      # all tab panels will have this padding, it indents the contents of a tab panel without effecting the top nav bar
                      style='padding-left:50px;',
                      # includeMarkdown("creating a Technomic Shiny App.Rmd")
                      fluidRow(
                        
                        column(12, 
                               column(6, 
                                      h2("") # include an appropriate page title
                               ),
                               column(6, # this is the loading graph for when shiny is working, can change the width of these columns if long title
                                      conditionalPanel(condition="$('html').hasClass('shiny-busy')",
                                                       img(src="Loading_200px.gif", height = "50px"))
                               )
                               
                        )
                      ),

                      DT::dataTableOutput("value_table")
                      
                      
                      
                      
             ), 
             
             tabPanel("Reason for value", 
                      # value should be the title of the tab followed by "_tab" this is used if any buttons update the current tab selected or to show/hide tabs
                      value = "reasonVal_tab",
                      # do not need icons for any tabs, this is optional. when an app has a homepage, the home icon is used.
                      #icon = icon("home"),
                      # all tab panels will have this padding, it indents the contents of a tab panel without effecting the top nav bar
                      style='padding-left:50px;',
                      # includeMarkdown("creating a Technomic Shiny App.Rmd")
                      fluidRow(
                        
                        column(12, 
                               column(6, 
                                      h2("") # include an appropriate page title
                               ),
                               column(6, # this is the loading graph for when shiny is working, can change the width of these columns if long title
                                      conditionalPanel(condition="$('html').hasClass('shiny-busy')",
                                                       img(src="Loading_200px.gif", height = "50px"))
                               )
                               
                        )
                      ),
                      
                      DT::dataTableOutput("reason_value_table")
                      
                      
                      
                      
             ),
             
             tabPanel("Quality", 
                      # value should be the title of the tab followed by "_tab" this is used if any buttons update the current tab selected or to show/hide tabs
                      value = "quality_tab",
                      # do not need icons for any tabs, this is optional. when an app has a homepage, the home icon is used.
                      #icon = icon("home"),
                      # all tab panels will have this padding, it indents the contents of a tab panel without effecting the top nav bar
                      style='padding-left:50px;',
                      # includeMarkdown("creating a Technomic Shiny App.Rmd")
                      fluidRow(
                        
                        column(12, 
                               column(6, 
                                      h2("") # include an appropriate page title
                               ),
                               column(6, # this is the loading graph for when shiny is working, can change the width of these columns if long title
                                      conditionalPanel(condition="$('html').hasClass('shiny-busy')",
                                                       img(src="Loading_200px.gif", height = "50px"))
                               )
                               
                        )
                      ),
                      
                      DT::dataTableOutput("quality_table")
                      
                      
                      
                      
             ),
             
             tabPanel("Draw", 
                      # value should be the title of the tab followed by "_tab" this is used if any buttons update the current tab selected or to show/hide tabs
                      value = "draw_tab",
                      # do not need icons for any tabs, this is optional. when an app has a homepage, the home icon is used.
                      #icon = icon("home"),
                      # all tab panels will have this padding, it indents the contents of a tab panel without effecting the top nav bar
                      style='padding-left:50px;',
                      # includeMarkdown("creating a Technomic Shiny App.Rmd")
                      fluidRow(
                        
                        column(12, 
                               column(6, 
                                      h2("") # include an appropriate page title
                               ),
                               column(6, # this is the loading graph for when shiny is working, can change the width of these columns if long title
                                      conditionalPanel(condition="$('html').hasClass('shiny-busy')",
                                                       img(src="Loading_200px.gif", height = "50px"))
                               )
                               
                        )
                      ),
                      
                      DT::dataTableOutput("draw_table")
                      
                      
                      
                      
             ),
             
             tabPanel("Repeat Trial", 
                      # value should be the title of the tab followed by "_tab" this is used if any buttons update the current tab selected or to show/hide tabs
                      value = "draw_tab",
                      # do not need icons for any tabs, this is optional. when an app has a homepage, the home icon is used.
                      #icon = icon("home"),
                      # all tab panels will have this padding, it indents the contents of a tab panel without effecting the top nav bar
                      style='padding-left:50px;',
                      # includeMarkdown("creating a Technomic Shiny App.Rmd")
                      fluidRow(
                        
                        column(12, 
                               column(6, 
                                      h2("") # include an appropriate page title
                               ),
                               column(6, # this is the loading graph for when shiny is working, can change the width of these columns if long title
                                      conditionalPanel(condition="$('html').hasClass('shiny-busy')",
                                                       img(src="Loading_200px.gif", height = "50px"))
                               )
                               
                        )
                      ),
                      
                      DT::dataTableOutput("repeat_trial_table")
                      
                      
                      
                      
             ),
             
             tabPanel("Craveability", 
                      # value should be the title of the tab followed by "_tab" this is used if any buttons update the current tab selected or to show/hide tabs
                      value = "Craveability_tab",
                      # do not need icons for any tabs, this is optional. when an app has a homepage, the home icon is used.
                      #icon = icon("home"),
                      # all tab panels will have this padding, it indents the contents of a tab panel without effecting the top nav bar
                      style='padding-left:50px;',
                      # includeMarkdown("creating a Technomic Shiny App.Rmd")
                      fluidRow(
                        
                        column(12, 
                               column(6, 
                                      h2("") # include an appropriate page title
                               ),
                               column(6, # this is the loading graph for when shiny is working, can change the width of these columns if long title
                                      conditionalPanel(condition="$('html').hasClass('shiny-busy')",
                                                       img(src="Loading_200px.gif", height = "50px"))
                               )
                               
                        )
                      ),
                      
                      DT::dataTableOutput("craveability_table")
                      
                      
                      
                      
             ),
             
             tabPanel("Additional Purchases", 
                      # value should be the title of the tab followed by "_tab" this is used if any buttons update the current tab selected or to show/hide tabs
                      value = "additional_purchases_tab",
                      # do not need icons for any tabs, this is optional. when an app has a homepage, the home icon is used.
                      #icon = icon("home"),
                      # all tab panels will have this padding, it indents the contents of a tab panel without effecting the top nav bar
                      style='padding-left:50px;',
                      # includeMarkdown("creating a Technomic Shiny App.Rmd")
                      fluidRow(
                        
                        column(12, 
                               column(6, 
                                      h2("") # include an appropriate page title
                               ),
                               column(6, # this is the loading graph for when shiny is working, can change the width of these columns if long title
                                      conditionalPanel(condition="$('html').hasClass('shiny-busy')",
                                                       img(src="Loading_200px.gif", height = "50px"))
                               )
                               
                        )
                      ),
                      
                      DT::dataTableOutput("additional_purchases_table")
                      
                      
                      
                      
             )
             
             
             
             
      
                      
             )
             
             
             
             
             
  )
